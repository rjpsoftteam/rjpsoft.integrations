﻿using RJPSoft.Integrations.Facebook.Model;
using System;
using System.Collections;
using UnityEngine;
using Windows.Security.Authentication.Web;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Methods

        private void ExecuteLogin()
        {
            UnityEngine.WSA.Application.InvokeOnUIThread(
                async () =>
                {
                    var result = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, new Uri(_loginUri, UriKind.Absolute),
                        WebAuthenticationBroker.GetCurrentApplicationCallbackUri());
                    Debug.Log($"[FacebookIntegrationService][ExecuteLogin] - Response Error Detail: {result.ResponseErrorDetail}");
                    var resultStatus = ParseWebAuthenticationStatusToResponseStatusCode(result.ResponseStatus);
                    FinishLogin(result.ResponseData, resultStatus);
                },
                true);
        }

        #endregion Private Methods
    }
}