﻿using System;
using System.Collections;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration. Not immplemented
    /// </summary>
    /// <seealso cref="Facebook.Singleton{FacebookIntegrationService}" />
    /// <exception cref="NotImplementedException">This method is not implemented</exception>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Methods

        IEnumerator ExecuteLogin()
        {
            throw new NotImplementedException("Use login from Facebook SDK for Unity. https://developers.facebook.com/docs/unity/");
        }

        #endregion Private Methods
    }
}