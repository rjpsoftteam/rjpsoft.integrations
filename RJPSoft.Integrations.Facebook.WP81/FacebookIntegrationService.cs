﻿using System;
using System.Collections;
using UnityEngine;
using Windows.Security.Authentication.Web;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Public Methods

        /// <summary>
        /// Finishes the login from continuation
        /// </summary>
        /// <param name="webAuthenticationResult"> result from authentication</param>
        /// <remarks>
        /// see https://docs.microsoft.com/en-us/uwp/api/Windows.Security.Authentication.Web.WebAuthenticationBroker#Windows_Security_Authentication_Web_WebAuthenticationBroker_AuthenticateAndContinue_Windows_Foundation_Uri_
        /// </remarks>
        public void LoginContinuation(WebAuthenticationResult webAuthenticationResult)
        {
            Debug.Log($"[FacebookIntegrationService][LoginContinuation] - Response Error Detail: {webAuthenticationResult.ResponseErrorDetail}");
            var responseStatusCode = ParseWebAuthenticationStatusToResponseStatusCode(webAuthenticationResult.ResponseStatus);
            FinishLogin(webAuthenticationResult.ResponseData, responseStatusCode);
        }

        #endregion Public Methods

        #region Private Methods

        private void ExecuteLogin()
        {
            UnityEngine.WSA.Application.InvokeOnUIThread(
                () =>
                {
                    WebAuthenticationBroker.AuthenticateAndContinue(new Uri(_loginUri, UriKind.Absolute),
                        WebAuthenticationBroker.GetCurrentApplicationCallbackUri());
                },
                true);
        }

        #endregion Private Methods
    }
}