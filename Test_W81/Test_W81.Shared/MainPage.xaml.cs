﻿#if WINDOWS_PHONE_APP
using Windows.ApplicationModel.Activation;
using Windows.Security.Authentication.Web;
#endif

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using RJPSoft.Integrations.Facebook;
using RJPSoft.Integrations.Facebook.EventArgs;
using System;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using RJPSoft.Integrations.Facebook.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Test_W81
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
#if WINDOWS_PHONE_APP
    public sealed partial class MainPage : Page, RJPSoft.Integrations.SuspensionHandlers.IWebAuthenticationContinuable
#else
    public sealed partial class MainPage : Page
#endif
    {
        FacebookIntegrationService _facebookService;

        public MainPage()
        {
            this.InitializeComponent();
            _facebookService = FacebookIntegrationService.GetInstance("196078490889366");
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            var callback = FacebookIntegrationService.GetApplicationCallbackUri();
            var permissions = new FacebookPermissions[] 
            {
                FacebookPermissions.PUBLIC_PROFILE,
                FacebookPermissions.USER_FRIENDS,
                FacebookPermissions.publish_actions,
                FacebookPermissions.user_photos
            };

            var loginUrl = _facebookService.GetLoginUri(callback.AbsoluteUri, permissions);
            _facebookService.LoginCompleted += _facebookService_LoginCompleted;
            _facebookService.LoginAsync(loginUrl);         
        }

        private void _facebookService_LoginCompleted(object sender, FacebookLoginEventArgs e)
        {
            textBlockSuccess.Text = e.Success.ToString();
            textBlockStatusCode.Text = e.StatusCode.ToString();

            if (e.Error != null)
            {
                textBlockError.Text = e.Error.Error;
                textBlockErrorDescription.Text = e.Error.ErrorDescription;
                textBlockErrorReason.Text = e.Error.ErrorReason;
            }
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            _facebookService.Logout();
        }

        private void userInfoButton_Click(object sender, RoutedEventArgs e)
        {
            _facebookService.GetUserInfoCompleted += GetUserInfoCompleted;
            _facebookService.GetUserInfoAsync();
        }

        private async void GetUserInfoCompleted(object sender, GetUserInfoEventArgs e)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () => 
            {
                if (e.StatusCode == ResponseStatusCode.Success)
                {
                    textBlockUserName.Text = e.User.FirstName;
                    textBlockImageUrl.Text = e.User.ProfilePictureBase64;
                    byte[] imageBytes = Convert.FromBase64String(e.User.ProfilePictureBase64);
                    // Convert byte[] to Image
                    using (InMemoryRandomAccessStream ms = new InMemoryRandomAccessStream())
                    {
                        using (DataWriter writer = new DataWriter(ms.GetOutputStreamAt(0)))
                        {
                            writer.WriteBytes(imageBytes);
                            writer.StoreAsync().GetResults();
                        }

                        var image = new BitmapImage();
                        await image.SetSourceAsync(ms); 
                        imageUser.Source = image;
                    }
                }
            });
        }

#if WINDOWS_PHONE_APP

        public void ContinueWebAuthentication(WebAuthenticationBrokerContinuationEventArgs args)
        {
            var responseStatusCode = FacebookIntegrationService.ParseWebAuthenticationStatusToResponseStatusCode(args.WebAuthenticationResult.ResponseStatus);
            _facebookService.LoginContinuation(args.WebAuthenticationResult.ResponseData, args.WebAuthenticationResult.ResponseErrorDetail, responseStatusCode);
        }
#endif
    }
}
