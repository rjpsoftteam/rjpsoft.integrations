﻿using System.Collections.Generic;

namespace RJPSoft.Integrations.Facebook.API.Services
{
    public sealed class DataTransportService : IDataTransportService
    {
        Dictionary<string, object> _data = new Dictionary<string, object>();

        public T Get<T>(string key)
        {
            return _data.ContainsKey(key) ? (T)_data[key] : default(T);
        }

        public void Add<T>(string key, T value)
        {
            _data[key] = value;
        }

    }
}
