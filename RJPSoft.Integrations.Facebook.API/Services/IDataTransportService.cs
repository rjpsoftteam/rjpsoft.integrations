﻿namespace RJPSoft.Integrations.Facebook.API.Services
{
    public interface IDataTransportService
    {
        void Add<T>(string key, T value);
        T Get<T>(string key);
    }
}