﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.API.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static RJPSoft.Integrations.Facebook.API.Constants;

namespace RJPSoft.Integrations.Facebook.API.Middlewares
{
    public class ValidationMiddleware
    {
        #region Private Fields

        private readonly RequestDelegate _next;

        #endregion Private Fields

        #region Public Constructors

        public ValidationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task Invoke(HttpContext httpContext)
        {
            var validationErrors = new List<string>();
            if (!httpContext.Request.Headers.ContainsKey(RequestHeaders.APP_ID))
            {
                validationErrors.Add(ErrorMessages.VALIDATION_MISSING_APP_ID);
            }
            if (!httpContext.Request.Headers.ContainsKey(RequestHeaders.ACCESS_TOKEN))
            {
                validationErrors.Add(ErrorMessages.VALIDATION_MISSING_ACCESS_TOKEN);
            }
            if (!httpContext.Request.Headers.ContainsKey(RequestHeaders.APP_VERSION))
            {
                validationErrors.Add(ErrorMessages.VALIDATION_MISSING_APP_VERSION);
            }

            if (validationErrors.Any())
            {
                var error = new RootApiError(validationErrors, ErrorMessages.VALIDATION_ERROR_TYPE, StatusCodes.Status422UnprocessableEntity);
                httpContext.Response.ContentType = Request.APPLICATION_JSON_CONTENT_TYPE;
                httpContext.Response.StatusCode = error.Error.Code;
                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(error));
            }
            else
            {
                await _next(httpContext);
            }
        }

        #endregion Public Methods
    }

    /// <summary>
    /// Extension method used to add the middleware to the HTTP request pipeline.
    /// </summary>
    public static class ValidationMiddlewareExtensions
    {
        #region Public Methods

        /// <summary>
        /// Extension method used to add the middleware to the HTTP request pipeline.
        /// </summary>
        /// <param name="builder">The <see cref="IApplicationBuilder"/> instance.</param>
        /// <returns>A <see cref="IApplicationBuilder"/> instance with the <see cref="ValidationMiddleware"/> added</returns>
        public static IApplicationBuilder UseValidationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ValidationMiddleware>();
        }

        #endregion Public Methods
    }
}