﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.API.Models;
using RJPSoft.Integrations.Facebook.API.Services;
using RJPSoft.Security.Cryptography;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using static RJPSoft.Integrations.Facebook.API.Constants;

namespace RJPSoft.Integrations.Facebook.API.Middlewares
{
    public class SecurityMiddleware
    {
        #region Private Fields

        readonly IConfigurationRoot _configuration;
        readonly RequestDelegate _next;

        #endregion Private Fields

        #region Public Constructors

        public SecurityMiddleware(RequestDelegate next, IConfigurationRoot configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task Invoke(HttpContext httpContext, IDataTransportService dataTransportService)
        {
            string rsaKey;
            string appId;
            string hashedAppId;

            var localIV = _configuration[Data.CONFIGURATION_APP_IV];
            var encryptedAppId = httpContext.Request.Headers[RequestHeaders.APP_ID].First();
            var encryptedAccessToken = httpContext.Request.Headers[RequestHeaders.ACCESS_TOKEN].First();
            var appVersion = httpContext.Request.Headers[RequestHeaders.APP_VERSION].First();

            //Not required for all end points
            var encryptedUserId = httpContext.Request.Headers[RequestHeaders.USER_ID].FirstOrDefault();

            var apiVersion = httpContext.Request.Path.Value.Split('/')[2]; // "[0]/api[1]/{version}[2]/..."

            var security = AppData.GetAppSecurityData(apiVersion);

            try
            {
                rsaKey = CryptoHandler.DecryptAES(security.RSAKey, Constants.Security.AES_KEY, localIV);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ErrorMessages.SECURITY_DECRIPT_RSA_KEY_LOG_MESSAGE, ex);
                await WriteErrorReponseAsync(httpContext, ErrorMessages.SECURITY_DECRIPT_RSA_KEY_RETURN_MESSAGE, StatusCodes.Status500InternalServerError);
                return;
            }

            try
            {
                appId = CryptoHandler.DecryptRSA(encryptedAppId, rsaKey);
                hashedAppId = CryptoHandler.ApplySha256(appId);
            }
            catch (Exception)
            {
                await WriteErrorReponseAsync(httpContext, ErrorMessages.SECURITY_UNPROCESSABLE_HEADER, StatusCodes.Status422UnprocessableEntity);
                return;
            }

            var app = AppData.GetFacebookApplicationData(hashedAppId);

            if (app != null)
            {
                try
                {
                    var accessToken = CryptoHandler.DecryptRSA(encryptedAccessToken, rsaKey);
                    var appSecret = CryptoHandler.DecryptAES(app.AppSecret, Constants.Security.AES_KEY, localIV);
                    var userId = !string.IsNullOrWhiteSpace(encryptedUserId) ? CryptoHandler.DecryptRSA(encryptedUserId, rsaKey) : string.Empty;

                    dataTransportService.Add(Data.APP_ID, appId);
                    dataTransportService.Add(Data.APP_SECRET, appSecret);
                    dataTransportService.Add(Data.USER_TOKEN, accessToken);
                    dataTransportService.Add(Data.USER_ID, userId);
                    dataTransportService.Add(Data.API_VERSION, apiVersion);
                }
                catch (Exception)
                {
                    await WriteErrorReponseAsync(httpContext, ErrorMessages.SECURITY_UNPROCESSABLE_HEADER, StatusCodes.Status422UnprocessableEntity);
                    return;
                }
            }
            else
            {
                await WriteErrorReponseAsync(httpContext, ErrorMessages.SECURITY_NO_API_VERSION_FOUND, StatusCodes.Status422UnprocessableEntity);
                return;
            }

            await _next(httpContext);
        }

        #endregion Public Methods

        #region Private Methods

        private static async Task WriteErrorReponseAsync(HttpContext httpContext, string message, int statusCode)
        {
            var error = new RootApiError(message, ErrorMessages.SECURITY_ERROR_TYPE, statusCode);
            httpContext.Response.ContentType = Request.APPLICATION_JSON_CONTENT_TYPE;
            httpContext.Response.StatusCode = error.Error.Code;
            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(error)).ConfigureAwait(false);
        }

        #endregion Private Methods
    }

    /// <summary>
    /// Extension method used to add the middleware to the HTTP request pipeline.
    /// </summary>
    public static class SecurityMiddlewareExtensions
    {
        #region Public Methods

        /// <summary>
        /// Extension method used to add the middleware to the HTTP request pipeline.
        /// </summary>
        /// <param name="builder">The <see cref="IApplicationBuilder"/> instance.</param>
        /// <returns>A <see cref="IApplicationBuilder"/> instance with the <see cref="ValidationMiddleware"/> added</returns>
        public static IApplicationBuilder UseSecurityMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SecurityMiddleware>();
        }

        #endregion Public Methods
    }
}