﻿using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.API.Repository.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace RJPSoft.Integrations.Facebook.API
{
    /// <summary>
    /// Holds application information
    /// </summary>
    public static class AppData
    {
        static readonly Dictionary<string, FacebookApplication> FACEBOOK_APPLICATION_DATA;
        static readonly Dictionary<string, AppSecurity> APPLICATION_SECURITY_DATA;

        static AppData()
        {
            var envName = Environment.GetEnvironmentVariable(Constants.Environment.ENVIRONMENT_NAME_KEY);

            var facebookApplicationDataText = File.ReadAllText($"fbApplicationData.{envName}.json");
            var appSecurityDataText = File.ReadAllText($"appSecurityData.{envName}.json");

            FACEBOOK_APPLICATION_DATA = JsonConvert.DeserializeObject<Dictionary<string, FacebookApplication>>(facebookApplicationDataText);
            APPLICATION_SECURITY_DATA = JsonConvert.DeserializeObject<Dictionary<string, AppSecurity>>(appSecurityDataText);
        }

        /// <summary>
        /// Gets the application security data.
        /// </summary>
        /// <param name="apiVersion">The API version.</param>
        /// <returns>Security information</returns>
        public static AppSecurity GetAppSecurityData(string apiVersion) =>
            APPLICATION_SECURITY_DATA.ContainsKey(apiVersion) ? APPLICATION_SECURITY_DATA[apiVersion] : null;

        /// <summary>
        /// Gets the facebook application data.
        /// </summary>
        /// <param name="hashedAppId">The hashed application identifier.</param>
        /// <returns>Facebook app information</returns>
        public static FacebookApplication GetFacebookApplicationData(string hashedAppId) =>
            FACEBOOK_APPLICATION_DATA.ContainsKey(hashedAppId) ? FACEBOOK_APPLICATION_DATA[hashedAppId] : null;
    }
}
