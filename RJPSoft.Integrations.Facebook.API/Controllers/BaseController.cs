﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RJPSoft.Integrations.Facebook.API.Models;
using RJPSoft.Integrations.Facebook.API.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static RJPSoft.Integrations.Facebook.API.Constants;

namespace RJPSoft.Integrations.Facebook.API.Controllers
{
    /// <summary>
    /// Base class for all the controllers
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    public class BaseController : Controller
    {
        #region Protected Fields

        protected readonly string APP_ID;
        protected readonly string APP_SECRET;
        protected readonly string SECRET_PROOF;
        protected readonly string TOKEN;
        protected readonly Dictionary<string, string> TOKEN_AND_SECRET_QUERY;
        protected readonly string USER_ID;
        protected readonly string VERSION;
        readonly IConfigurationRoot _configuration;
        readonly IDataTransportService _dataTransportService;

        #endregion Protected Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="dataTransportService">The data transport service.</param>
        public BaseController(IConfigurationRoot configuration, IDataTransportService dataTransportService)
        {
            _configuration = configuration;
            _dataTransportService = dataTransportService;
            APP_ID = _dataTransportService.Get<string>(Data.APP_ID);
            VERSION = _dataTransportService.Get<string>(Data.APP_VERSION);
            TOKEN = _dataTransportService.Get<string>(Data.USER_TOKEN);
            APP_SECRET = _dataTransportService.Get<string>(Data.APP_SECRET);
            USER_ID = _dataTransportService.Get<string>(Data.USER_ID);
            SECRET_PROOF = GenerateSecretProof();
            TOKEN_AND_SECRET_QUERY = new Dictionary<string, string>
            {
                { Constants.Request.FB_REQUEST_TOKEN_HEADER_KEY, TOKEN },
                { Constants.Request.FB_REQUEST_PROOF_HEADER_KEY, SECRET_PROOF }
            };
        }

        #endregion Public Constructors

        #region Protected Methods

        /// <summary>
        /// Gets the user gender asynchronous.
        /// </summary>
        /// <param name="client">The httpClient.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="tokenAndSecretQuery">The token and secret query.</param>
        /// <returns>The user gender</returns>
        protected static async Task<string> GetUserGenderAsync(HttpClient client, string userId, Dictionary<string, string> tokenAndSecretQuery)
        {
            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{userId}{Get.USER_GENDER_END_POINT}";
            var uri = QueryHelpers.AddQueryString(url, tokenAndSecretQuery);
            var user = await HandleGetAsync<User>(client, uri);
            return user.Gender;
        }

        /// <summary>
        /// Gets the user profile picture as a base64 string.
        /// </summary>
        /// <param name="client">The client the will execute the GET</param>
        /// <param name="userId">The user id to GET the picture from</param>
        /// <param name="tokenAndSecretQuery">The params that will be passed over the query string</param>
        /// <returns>The picture as a base64 string</returns>
        /// <exception cref="HttpRequestException">Get exception</exception>
        protected static async Task<string> GetUserProfilePictureBase64Async(HttpClient client, string userId, Dictionary<string, string> tokenAndSecretQuery)
        {
            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{userId}";
            var picture = await HandleGetPictureAsync(client, url, tokenAndSecretQuery);
            return picture;
        }

        /// <summary>
        /// Gets the user profile picture as a base64 string.
        /// </summary>
        /// <param name="client">The <see cref="HttpClient"/> that will be used to get de image</param>
        /// <param name="url">The URL to download the picture.</param>
        /// <returns>The picture as a base64 string</returns>
        /// <exception cref="HttpRequestException">Get exception</exception>
        protected static async Task<string> GetUserProfilePictureBase64Async(HttpClient client, string url)
        {
            string result = null;
            if (!string.IsNullOrWhiteSpace(url))
            {
                HttpResponseMessage response = null;
                try
                {
                    response = await client.GetAsync(url);
                }
                catch (Exception ex)
                {
                    var error = new RootApiError(ex.Message, ErrorMessages.GET_PICTURE_ERROR_TYPE, StatusCodes.Status400BadRequest);
                    Debug.WriteLine(ex);
                    throw new HttpRequestException(JsonConvert.SerializeObject(error));
                }

                if (response.IsSuccessStatusCode)
                {
                    var getResult = await response.Content.ReadAsByteArrayAsync();
                    result = Convert.ToBase64String(getResult);
                }
                else
                {
                    var getResult = await response.Content.ReadAsStringAsync();
                    Debug.WriteLine(getResult);
                    throw new HttpRequestException(getResult);
                }
            }

            return result;
        }

        /// <summary>
        /// Executes a GET operation.
        /// </summary>
        /// <typeparam name="T">The type of the GET result</typeparam>
        /// <param name="uri">The URI to execute the GET.</param>
        /// <returns>A object of type <typeparamref name="T"/></returns>
        protected static async Task<T> HandleGetAsync<T>(string uri)
        {
            using (var client = new HttpClient())
            {
                return await PrivateGetAsync<T>(uri, client);
            }
        }

        /// <summary>
        /// Executes a GET operation.
        /// </summary>
        /// <typeparam name="T">The type of the GET result</typeparam>
        /// <param name="client">The client tha will execute the GET.</param>
        /// <param name="uri">The URI to execute the GET.</param>
        /// <returns>A object of type <typeparamref name="T"/></returns>
        protected static async Task<T> HandleGetAsync<T>(HttpClient client, string uri) => await PrivateGetAsync<T>(uri, client);

        /// <summary>
        /// Gets the user profile picture when the profile picture URL is not known.
        /// If the profile picture URL is known, use <seealso cref="GetUserProfilePictureBase64Async(HttpClient, string)"/>
        /// </summary>
        /// <param name="client">The client to meke the GET.</param>
        /// <param name="url">The URL to make the GET.</param>
        /// <param name="query">The params thet will be put on query string.</param>
        /// <returns>Picture as base64</returns>
        /// <exception cref="HttpRequestException">the GET exception</exception>
        protected static async Task<string> HandleGetPictureAsync(HttpClient client, string url, Dictionary<string, string> query)
        {
            var uri = QueryHelpers.AddQueryString($"{url}{Get.PICTURE_END_POINT}", query);
            var picture = await GetUserProfilePictureBase64Async(client, uri);
            return picture;
        }

        /// <summary>
        /// Generates the secret proof required by faccebook to make server calls to graph API.
        /// </summary>
        /// <returns>The secrete proof</returns>
        protected string GenerateSecretProof()
        {
            var secretByte = Encoding.UTF8.GetBytes(APP_SECRET);
            var tokenBytes = Encoding.UTF8.GetBytes(TOKEN);

            using (var hmacsha256 = new HMACSHA256(secretByte))
            {
                var hash = hmacsha256.ComputeHash(tokenBytes);
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
        }

        /// <summary>
        /// Executes a POST to the specifyed <paramref name="uri"/>
        /// </summary>
        /// <param name="uri">The URI to meke the POST to.</param>
        /// <param name="content">The content to POST.</param>
        /// <returns>The post result</returns>
        /// <exception cref="HttpRequestException">POST exception</exception>
        protected async Task<IActionResult> HandlePostAsync(string uri, HttpContent content)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = null;
                string result = null;

                try
                {
                    response = await client.PostAsync(uri, content);
                    result = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ex)
                {
                    var error = new RootApiError(ex.Message, ErrorMessages.FB_POST_REQUEST_ERROR_TYPE, StatusCodes.Status400BadRequest);
                    throw new HttpRequestException(JsonConvert.SerializeObject(error));
                }

                return response != null && response.IsSuccessStatusCode ?
                    Ok(JObject.Parse(result)) : StatusCode((int)response.StatusCode, JObject.Parse(result));
            }
        }

        #endregion Protected Methods

        #region Private Methods

        static async Task<T> PrivateGetAsync<T>(string uri, HttpClient client)
        {
            HttpResponseMessage response = null;
            string result = null;

            try
            {
                response = await client.GetAsync(uri);
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                var error = new RootApiError(ex.Message, ErrorMessages.FB_GET_REQUEST_ERROR_TYPE, StatusCodes.Status400BadRequest);
                throw new HttpRequestException(JsonConvert.SerializeObject(error));
            }

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<T>(result);
            }
            else
            {
                throw new HttpRequestException(result);
            }
        }

        #endregion
    }
}