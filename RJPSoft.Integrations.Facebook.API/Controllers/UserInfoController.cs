﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.API.Models;
using RJPSoft.Integrations.Facebook.API.Services;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using static RJPSoft.Integrations.Facebook.API.Constants;

namespace RJPSoft.Integrations.Facebook.API.Controllers
{
    /// <summary>
    /// Holds API methods to deal with user info
    /// </summary>
    /// <seealso cref="BaseController" />
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class UserInfoController : BaseController
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserInfoController"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public UserInfoController(IConfigurationRoot configuration, IDataTransportService dataTransportService)
            : base(configuration, dataTransportService)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Gets the user info.
        /// </summary>
        /// <returns>All the user info requested in the query string</returns>
        [HttpGet, MapToApiVersion("1.0")]
        public async Task<IActionResult> GetAsync()
        {
            User result = null;
            try
            {
                result = await GetUserInfoAsync();
            }
            catch (HttpRequestException hre)
            {
                Response.ContentType = Constants.Request.APPLICATION_JSON_CONTENT_TYPE;
                var error = JsonConvert.DeserializeObject<RootApiError>(hre.Message);
                if (error != null)
                {
                    Response.StatusCode = error.Error.Code;
                    await Response.WriteAsync(hre.Message);
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    var json = new RootApiError(hre.Message, ErrorMessages.UNEXPECTED_ERROR_TYPE, StatusCodes.Status503ServiceUnavailable);
                    await Response.WriteAsync(JsonConvert.SerializeObject(json));
                }
            }

            return Ok(result);
        }

        #endregion Public Methods

        #region Private Methods

        private async Task<User> GetUserInfoAsync()
        {
            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{Constants.Get.USER_INFO_END_POINT}{Request.QueryString}";
            var uri = QueryHelpers.AddQueryString(url, TOKEN_AND_SECRET_QUERY);
            using (var client = new HttpClient())
            {
                var result = await HandleGetAsync<User>(client, uri);

                try
                {
                    result.ProfilePictureBase64 = await GetUserProfilePictureBase64Async(client, result.ProfilePictureUrl);
                }
                catch (HttpRequestException hre)
                {
                    //If an error occours while getting the image, we just return the user without an image
                    Debug.WriteLine("Get Profile Picture", hre);
                }

                return result;

            }

            #endregion Private Methods
        }
    }
}