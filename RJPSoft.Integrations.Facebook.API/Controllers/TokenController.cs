﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RJPSoft.Integrations.Facebook.API.Controllers
{
    /// <summary>
    /// Controller for token requests
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class TokenController : Controller
    {
        #region Private Fields

        private const string BASE_TOKEN_URL = "https://graph.facebook.com/oauth/access_token";

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Gets the facebook long living token
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="shortToken">The short living token.</param>
        /// <returns>The lognliving facebook token</returns>
        [HttpGet("{shortToken}")]
        public async Task<IActionResult> Get([FromServices] IConfigurationRoot configuration, string shortToken)
        {
            var clientId = configuration.GetValue<string>("fb_client");
            var clientSecret = configuration.GetValue<string>("fb_secret");

            var uri = new Uri(BASE_TOKEN_URL + "?" +
                "grant_type=fb_exchange_token" +
                $"&client_id={clientId}" +
                $"&client_secret={clientSecret}" +
                $"&fb_exchange_token={shortToken}");

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(uri);
                var content = await response.Content.ReadAsStringAsync();
                return Ok(content);
            }
        }

        #endregion Public Methods
    }
}