﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RJPSoft.Integrations.Facebook.API.Models;
using RJPSoft.Integrations.Facebook.API.Services;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static RJPSoft.Integrations.Facebook.API.Constants;

namespace RJPSoft.Integrations.Facebook.API.Controllers
{
    /// <summary>
    /// Controller that handles score operations
    /// </summary>
    /// <seealso cref="RJPSoft.Integrations.Facebook.API.Controllers.BaseController" />
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class ScoreController : BaseController
    {
        #region Private Fields

        private const string POSITIVE_NUMBER_VALIDATION_MESSAGE = " must be greater then 0";
        private const string USER_ID_VALIDATION_MESSAGE = "Missing User Id";

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreController"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="dataTransportService">The data transport service.</param>
        public ScoreController(IConfigurationRoot configuration, IDataTransportService dataTransportService)
            : base(configuration, dataTransportService)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Gets the user score asynchronous.
        /// </summary>
        /// <returns>The user score</returns>
        [HttpGet, MapToApiVersion("1.0")]
        public async Task<IActionResult> GetUserScoreAsync()
        {
            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{Get.USER_HIGHSCORE}";
            var uri = QueryHelpers.AddQueryString(url, TOKEN_AND_SECRET_QUERY);
            Score result = null;

            try
            {
                using (var client = new HttpClient())
                {
                    var apiresult = await HandleGetAsync<DataContainer<Score[]>>(client, uri);
                    result = apiresult.Data.Any() ? apiresult.Data.Aggregate((s1, s2) => s1.Value > s2.Value ? s1 : s2) : new Score { Value = 0 };
                }
            }
            catch (HttpRequestException hre)
            {
                Response.ContentType = Constants.Request.APPLICATION_JSON_CONTENT_TYPE;
                var error = JsonConvert.DeserializeObject<RootApiError>(hre.Message);
                if (error != null)
                {
                    Response.StatusCode = error.Error.Code;
                    await Response.WriteAsync(hre.Message);
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    var json = new RootApiError(hre.Message, ErrorMessages.UNEXPECTED_ERROR_TYPE, StatusCodes.Status503ServiceUnavailable);
                    await Response.WriteAsync(JsonConvert.SerializeObject(json));
                }
            }

            return new JsonResult(result);
        }

        /// <summary>
        /// Gets the top and arround me asynchronous.
        /// </summary>
        /// <param name="qnt">The QNT.</param>
        /// <param name="arroundMe">The arround me.</param>
        /// <returns>The top users and the closest to the loged user</returns>
        [HttpGet("top/{qnt}/arroundMe/{arroundMe}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> GetTopAndarroundMeAsync(int qnt, int arroundMe)
        {
            var message = ValidateRequestData(qnt, arroundMe);
            if (message != null)
            {
                return BadRequest(message);
            }

            var allScores = (await GetScoresAsync()).ToList();
            var result = FilterResult(qnt, arroundMe, allScores);
            var usersToGetInfo = result.Where(s => s.User.Id != USER_ID);
            await GetProfilePictureAsBase64Async(usersToGetInfo);
            await GetGenderAsync(usersToGetInfo);

            return Ok(result);
        }

        /// <summary>
        /// Gets the top users asynchronous.
        /// </summary>
        /// <param name="qnt">The QNT.</param>
        /// <returns>The user with the greatest score</returns>
        [HttpGet("top/{qnt}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> GetTopAsync(int qnt)
        {
            if (qnt <= 0)
            {
                return BadRequest(
                    new RootApiError($"{nameof(qnt)}{POSITIVE_NUMBER_VALIDATION_MESSAGE}",
                    ErrorMessages.VALIDATION_ERROR_TYPE, StatusCodes.Status422UnprocessableEntity));
            }

            var result = (await GetScoresAsync()).ToList();
            var top = result.Take(qnt);
            await GetProfilePictureAsBase64Async(top);
            return Ok(top);
        }

        /// <summary>
        /// Posts the user score asynchronous.
        /// </summary>
        /// <param name="userScore">The user score.</param>
        /// <returns>Result code</returns>
        [HttpPost("{userScore}"), MapToApiVersion("1.0")]
        public async Task<IActionResult> PostAsync(int userScore)
        {
            if (userScore < 0)
            {
                return BadRequest(
                    new RootApiError($"{nameof(userScore)}{POSITIVE_NUMBER_VALIDATION_MESSAGE}", ErrorMessages.VALIDATION_ERROR_TYPE,
                    StatusCodes.Status422UnprocessableEntity));
            }

            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{Post.USER_HIGHSCORE}";
            var uri = QueryHelpers.AddQueryString(url, TOKEN_AND_SECRET_QUERY);

            var o = JObject.FromObject(new
            {
                score = userScore.ToString()
            });
            var content = new StringContent(o.ToString(), Encoding.UTF8, Constants.Request.APPLICATION_JSON_CONTENT_TYPE);

            IActionResult postResult = null;

            try
            {
                postResult = await HandlePostAsync(uri, content);
            }
            catch (HttpRequestException hre)
            {
                Response.ContentType = Constants.Request.APPLICATION_JSON_CONTENT_TYPE;
                var error = JsonConvert.DeserializeObject<RootApiError>(hre.Message);
                if (error != null)
                {
                    Response.StatusCode = error.Error.Code;
                    await Response.WriteAsync(hre.Message);
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    var json = new RootApiError(hre.Message, ErrorMessages.UNEXPECTED_ERROR_TYPE, StatusCodes.Status503ServiceUnavailable);
                    await Response.WriteAsync(JsonConvert.SerializeObject(json));
                }
            }

            return postResult;
        }

        #endregion Public Methods

        #region Private Methods

        private static List<Score> FilterTop(int qnt, List<Score> allScores)
        {
            var result = allScores.Take(qnt).ToList();
            for (var i = 0; i < result.Count(); i++)
            {
                result.ElementAt(i).Position = i + 1;
            }

            return result;
        }

        private List<Score> FilterArroundMe(List<Score> allScores, int value)
        {
            var myIndex = allScores.IndexOf(allScores.First(s => s.User.Id == USER_ID));
            var minIndex = myIndex - value <= 0 ? 0 : myIndex - value;
            var maxIndex = myIndex + value >= allScores.Count() ? allScores.Count() : myIndex + value;

            var arroundResult = new List<Score>();
            for (var i = minIndex; i < maxIndex; i++)
            {
                var element = allScores.ElementAt(i);
                element.Position = i + 1;
                arroundResult.Add(element);
            }

            return arroundResult;
        }

        private IEnumerable<Score> FilterResult(int qnt, int value, List<Score> allScores)
        {
            var result = FilterTop(qnt, allScores);

            var arroundResult = FilterArroundMe(allScores, value);

            return result.Union(arroundResult);
        }

        private async Task GetGenderAsync(IEnumerable<Score> scores)
        {
            using (var client = new HttpClient())
            {
                var tasks = new List<Task>();

                foreach (var score in scores)
                {
                    var task = GetUserGenderAsync(client, score.User.Id, TOKEN_AND_SECRET_QUERY)
                        .ContinueWith((taskResult) =>
                        {
                            if (!taskResult.IsFaulted && taskResult.IsCompleted)
                            {
                                score.User.Gender = taskResult.Result;
                                return taskResult.Result;
                            }
                            else
                            {
                                taskResult.Exception.Handle((e) => true);
                                Debug.WriteLine("Get user gender", taskResult.Exception);
                                return string.Empty;
                            }
                        });
                    tasks.Add(task);
                }

                await Task.WhenAll(tasks.ToArray());
            }
        }

        private async Task GetProfilePictureAsBase64Async(IEnumerable<Score> scores)
        {
            using (var client = new HttpClient())
            {
                var tasks = new List<Task>();
                foreach (var score in scores)
                {
                    var task = GetUserProfilePictureBase64Async(client, score.User.Id, TOKEN_AND_SECRET_QUERY)
                        .ContinueWith((taskResult) =>
                        {
                            if (!taskResult.IsFaulted && taskResult.IsCompleted)
                            {
                                score.User.ProfilePictureBase64 = taskResult.Result;
                                return taskResult.Result;
                            }
                            else
                            {
                                taskResult.Exception.Handle((e) => true);
                                Debug.WriteLine("Get Profile Picture" ,taskResult.Exception);
                                return string.Empty;
                            }
                        });
                    tasks.Add(task);
                }

                await Task.WhenAll(tasks.ToArray());
            }
        }

        private async Task<Score[]> GetScoresAsync()
        {
            var url = $"{Constants.Request.FACEBOOK_BASE_URL}{APP_ID}{Get.FRIENDS_HIGHSCORES}";
            var uri = QueryHelpers.AddQueryString(url, TOKEN_AND_SECRET_QUERY);
            DataContainer<Score[]> result = null;

            try
            {
                using (var client = new HttpClient())
                {
                    result = await HandleGetAsync<DataContainer<Score[]>>(client, uri);
                }
            }
            catch (HttpRequestException hre)
            {
                Response.ContentType = Constants.Request.APPLICATION_JSON_CONTENT_TYPE;
                var error = JsonConvert.DeserializeObject<RootApiError>(hre.Message);
                if (error != null)
                {
                    Response.StatusCode = error.Error.Code;
                    await Response.WriteAsync(hre.Message);
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    var json = new RootApiError(hre.Message, ErrorMessages.UNEXPECTED_ERROR_TYPE, StatusCodes.Status503ServiceUnavailable);
                    await Response.WriteAsync(JsonConvert.SerializeObject(json));
                }
            }

            return result.Data;
        }

        private object ValidateRequestData(int qnt, int arroundMe)
        {
            object result = null;

            if (qnt <= 0)
            {
                result = new RootApiError($"{nameof(qnt)}{POSITIVE_NUMBER_VALIDATION_MESSAGE}", ErrorMessages.VALIDATION_ERROR_TYPE,
                    StatusCodes.Status422UnprocessableEntity);
            }

            if (arroundMe <= 0)
            {
                result = new RootApiError($"{nameof(arroundMe)}{POSITIVE_NUMBER_VALIDATION_MESSAGE}", ErrorMessages.VALIDATION_ERROR_TYPE,
                    StatusCodes.Status422UnprocessableEntity);
            }

            if (string.IsNullOrWhiteSpace(USER_ID))
            {
                result = new RootApiError($"{USER_ID_VALIDATION_MESSAGE}", ErrorMessages.VALIDATION_ERROR_TYPE,
                   StatusCodes.Status422UnprocessableEntity);
            }

            return result;
        }

        #endregion Private Methods
    }
}