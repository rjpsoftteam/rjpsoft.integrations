﻿namespace RJPSoft.Integrations.Facebook.API
{
    /// <summary>
    /// Holds contants values
    /// </summary>
    internal static class Constants
    {
        #region Internal Classes

        /// <summary>
        /// Holds keys for the DataTransportService
        /// </summary>
        internal static class Data
        {
            #region Internal Fields

            internal const string APP_ID = "app_id";
            internal const string APP_SECRET = "app_secret";
            internal const string APP_VERSION = "app_version";
            internal const string USER_ID = "fb_user_id";
            internal const string USER_TOKEN = "user_token";
            internal const string API_VERSION = "api_version";

            internal const string CONFIGURATION_APP_IV = "Apps:IV";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds the application error messages
        /// </summary>
        internal static class ErrorMessages
        {
            #region Internal Fields

            internal const string FB_GET_REQUEST_ERROR_TYPE = "GET Request to facebook";
            internal const string FB_POST_REQUEST_ERROR_TYPE = "POST Request to facebook";
            internal const string FB_REQUEST_PICTURE_ERROR_TYPE = "Request profile picture url to facebook";
            internal const string GET_PICTURE_ERROR_TYPE = "Download profile picture";

            internal const string VALIDATION_ERROR_TYPE = "Validation";
            internal const string VALIDATION_MISSING_APP_ID = "Missing app id";
            internal const string VALIDATION_MISSING_ACCESS_TOKEN = "Missing access token";
            internal const string VALIDATION_MISSING_APP_VERSION = "Missing application version";

#pragma warning disable CC0021 // Use nameof
            internal const string SECURITY_ERROR_TYPE = "Security";
#pragma warning restore CC0021 // Use nameof
            internal const string SECURITY_UNPROCESSABLE_HEADER = "Unable to proccess request headers";
            internal const string SECURITY_NO_API_VERSION_FOUND = "This api version is not registered";
            internal const string SECURITY_DECRIPT_RSA_KEY_LOG_MESSAGE = "Decrypt RSA key error";
            internal const string SECURITY_DECRIPT_RSA_KEY_RETURN_MESSAGE = "internal data error";
            internal const string SECURITY_SECIRITY_TABLE_ENTRY_NOT_FOUND_MESSAGE = "No application found for the combination {0}:{1} and API version (at the path):{2}";

            internal const string UNEXPECTED_ERROR_TYPE = "Unexpected";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds constants for GET operations
        /// </summary>
        internal static class Get
        {
            #region Internal Fields

            internal const string FRIENDS_HIGHSCORES = "/scores";
            internal const string PICTURE_END_POINT = "/picture";
            internal const string USER_HIGHSCORE = "/me/scores";
            internal const string USER_INFO_END_POINT = "me";
            internal const string USER_GENDER_END_POINT = "?fields=gender";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds constants for POST operations
        /// </summary>
        internal static class Post
        {
            #region Internal Fields

            internal const string USER_HIGHSCORE = "me/scores";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds request constants
        /// </summary>
        internal static class Request
        {
            #region Internal Fields

            internal const string APPLICATION_JSON_CONTENT_TYPE = "application/json";
            internal const string FACEBOOK_BASE_URL = "https://graph.facebook.com/";
            internal const string FB_REQUEST_PROOF_HEADER_KEY = "appsecret_proof";
            internal const string FB_REQUEST_TOKEN_HEADER_KEY = "access_token";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds reqeust headers constants
        /// </summary>
        internal static class RequestHeaders
        {
            #region Internal Fields

            internal const string ACCESS_TOKEN = "fb_token";
            internal const string APP_ID = "fb_app_id";
            internal const string APP_VERSION = "app_version";
            internal const string USER_ID = "fb_user_id";

            #endregion Internal Fields
        }

        /// <summary>
        /// Holds security constants
        /// </summary>
        internal static class Security
        {
            #region Internal Fields

            internal static readonly string AES_KEY;

            #endregion Internal Fields

            #region Private Fields

            const string DEV_AES_KEY = "sONa/QbOiqb1lBct3ebzVRQqexf5q+862hQQpzluQuk=";
            const string PROD_AES_KEY = "pjwotG89ogwo3sRUjJWJO1Y0h+ntFRqbpYlvxgroHdk=";

            #endregion

            static Security()
            {
                var isProd = System.Environment.GetEnvironmentVariable(Environment.ENVIRONMENT_NAME_KEY) == Environment.PRODUCTION;
                AES_KEY = isProd ? PROD_AES_KEY : DEV_AES_KEY;
            }
        }

        /// <summary>
        /// holds environment constants
        /// </summary>
        internal static class Environment
        {
            internal const string DEVELOPMENT_REMOTE = "development_remote";
            internal const string DEVELOPMENT = "developmet";
            internal const string PRODUCTION = "production";
            internal const string ENVIRONMENT_NAME_KEY = "EnvironmentName";
        }

        #endregion Internal Classes
    }
}