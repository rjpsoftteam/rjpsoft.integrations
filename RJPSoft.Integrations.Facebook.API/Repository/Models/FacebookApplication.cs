﻿namespace RJPSoft.Integrations.Facebook.API.Repository.Models
{

    public sealed class FacebookApplication
    {
        public string AppId { get; set; }

        public string AppSecret { get; set; }
    }
}
