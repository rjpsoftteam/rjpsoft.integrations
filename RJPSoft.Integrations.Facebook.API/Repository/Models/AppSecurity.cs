﻿namespace RJPSoft.Integrations.Facebook.API.Repository.Models
{
    /// <summary>
    /// Holds the application security data
    /// </summary>
    public sealed class AppSecurity
    {
        /// <summary>
        /// Gets or sets the API version.
        /// </summary>
        public string ApiVersion { get; set; }

        /// <summary>
        /// Gets or sets the RSA key.
        /// </summary>
        public string RSAKey { get; set; }
    }
}
