﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    /// <summary>
    /// Holds the picture data information
    /// </summary>
    public sealed class Picture
    {
        /// <summary>
        /// Gets or sets the picture URL.
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
