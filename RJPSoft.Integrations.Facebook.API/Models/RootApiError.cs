﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    /// <summary>
    /// Root Container for API error
    /// </summary>
    [JsonObject]
    public class RootApiError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RootApiError"/> class.
        /// </summary>
        public RootApiError()
        {
            //empty for seralization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RootApiError"/> class.
        /// </summary>
        /// <param name="error">The API error.</param>
        public RootApiError(APIError error)
        {
            Error = error;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RootApiError"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="type">The error type.</param>
        /// <param name="code">The error code.</param>
        public RootApiError(string message, string type, int code)
        {
            Error = new APIError(message, type, code);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RootApiError"/> class.
        /// </summary>
        /// <param name="messages">The error messages.</param>
        /// <param name="type">The error type.</param>
        /// <param name="code">The error code.</param>
        public RootApiError(IEnumerable<string> messages, string type, int code)
        {
            Error = new APIError(messages, type, code);
        }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        [JsonProperty("error")]
        public APIError Error { get; set; }
    }
}
