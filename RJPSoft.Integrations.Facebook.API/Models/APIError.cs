﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    /// <summary>
    /// Holds information about the API error
    /// </summary>
    [JsonObject]
    public class APIError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="APIError"/> class.
        /// </summary>
        public APIError()
        {
            // empty for serialization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="APIError"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="type">The error type.</param>
        /// <param name="code">The error code.</param>
        public APIError(string message, string type, int code)
        {
            Message = message;
            Type = type;
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="APIError"/> class.
        /// </summary>
        /// <param name="messages">The error messages.</param>
        /// <param name="type">The error type.</param>
        /// <param name="code">The error code.</param>
        public APIError(IEnumerable<string> messages, string type, int code)
        {
            Message = string.Join("; ", messages);
            Type = type;
            Code = code;
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the error type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; }
    }
}