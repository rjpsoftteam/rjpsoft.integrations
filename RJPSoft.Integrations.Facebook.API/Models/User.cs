﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    /// <summary>
    /// HOlds Ueser information
    /// </summary>
    [JsonObject]
    public class User
    {
        #region Private Fields

        [JsonProperty("picture", NullValueHandling = NullValueHandling.Ignore)]
        private DataContainer<Picture> _profilePictureUrl = null;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [JsonProperty("first_name", NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the user gender.
        /// </summary>
        [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [JsonProperty("last_name", NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the name of the middle.
        /// </summary>
        [JsonProperty("middle_name", NullValueHandling = NullValueHandling.Ignore)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the profile picture encode as base64.
        /// </summary>
        [JsonProperty("profile_picture_base_64", NullValueHandling = NullValueHandling.Ignore)]
        public string ProfilePictureBase64 { get; set; }

        /// <summary>
        /// Gets the profile picture URL.
        /// </summary>
        [JsonIgnore]
        public string ProfilePictureUrl => _profilePictureUrl?.Data.Url;

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Equalses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns><c>true</c> if are the same object. <c>fase</c> otherwise</returns>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var otehrUser = (User)obj;
            return Id == otehrUser.Id;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Retuns a string that represents this instance
        /// </summary>
        /// <returns>A string</returns>
        public override string ToString() => $"{FullName} \n {ProfilePictureUrl}";

        #endregion Public Methods
    }
}