﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    [JsonObject]
    public class DataContainer<T>
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        [JsonProperty("data")]
        public T Data { get; set; }
    }
}
