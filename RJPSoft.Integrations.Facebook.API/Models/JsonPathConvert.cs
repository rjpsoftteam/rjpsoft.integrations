﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RJPSoft.Integrations.Facebook.API.Models
{
    /// <summary>
    /// Implements methods that allow <see cref="JsonConvert.DeserializeObject{T}(string)"/> to use property names in dot notation
    /// </summary>
    /// <example>
    ///     {
    ///         "prop1": {
    ///             "prop2": {
    ///                 "prop3": "bisteca"
    ///             }
    ///         }
    ///     }
    ///     can be mapped to:
    ///     [JsonConverter(typeof(JsonPathConverter))]
    ///     class User
    ///     {
    ///         [JsonProperty("prop1.prop2.prop3")]
    ///         internal string Prop3 { get; private set; }
    ///     }
    ///     Prop3 will have it's value = "bisteca"
    /// </example>
    public class JsonPathConverter : JsonConverter
    {
        /// <summary>
        /// Gets a value indicating whether this instance can write.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can write; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// it will always return <c>false</c>
        /// </remarks>
        public override bool CanWrite => false;

        /// <summary>
        /// Reads the json and convert to the especified type.
        /// </summary>
        /// <param name="reader">The json reader.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="serializer">The serializer.</param>
        /// <returns>A instace of <paramref name="objectType"/></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            object targetObj = Activator.CreateInstance(objectType);
            List<MemberInfo> members = new List<MemberInfo>();
#if NETFX_CORE
            members.AddRange(objectType.GetRuntimeProperties());
            members.AddRange(objectType.GetRuntimeFields());
#else
            members.AddRange(objectType.GetProperties());
            members.AddRange(objectType.GetFields());
#endif

            foreach (MemberInfo memberInfo in members)
            {
                var att = memberInfo.GetCustomAttributes(true)
                                                .OfType<JsonPropertyAttribute>()
                                                .FirstOrDefault();

                string jsonPath = att != null ? att.PropertyName : memberInfo.Name;
                JToken token = jo.SelectToken(jsonPath);

                if (token != null && token.Type != JTokenType.Null)
                {
                    if (memberInfo is PropertyInfo)
                    {
                        var prop = memberInfo as PropertyInfo;
                        object value = token.ToObject(prop.PropertyType, serializer);
                        prop.SetValue(targetObj, value, null);
                    }
                    else if (memberInfo is FieldInfo)
                    {
                        var fieldInfo = memberInfo as FieldInfo;
                        object value = token.ToObject(fieldInfo.FieldType, serializer);
                        fieldInfo.SetValue(targetObj, value);
                    }
                }
            }

            return targetObj;
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        ///   <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>
        /// CanConvert is not called when [JsonConverter] attribute is used
        /// it will always return <c>false</c>
        /// </remarks>
        public override bool CanConvert(Type objectType) => false;

        /// <summary>
        /// Writes the json.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The serializer.</param>
        /// <exception cref="NotImplementedException">Method not implemented</exception>
        /// <remarks>
        /// Not implemented
        /// </remarks>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}