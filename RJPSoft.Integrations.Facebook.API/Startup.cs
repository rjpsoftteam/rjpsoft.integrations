﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RJPSoft.Integrations.Facebook.API.Middlewares;
using RJPSoft.Integrations.Facebook.API.Services;
using System;

namespace RJPSoft.Integrations.Facebook.API
{
    /// <summary>
    /// Handle Startup
    /// </summary>
    public class Startup
    {
        readonly IHostingEnvironment _hostingEnvironment;

        /// <summary>
        /// Creates a new instance of <see cref="Startup"/>
        /// </summary>
        /// <param name="env">The environment</param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            _hostingEnvironment = env;
        }

        /// <summary>
        /// Gets the configuration
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// Cofigure Service
        /// </summary>
        /// <param name="services">Services to configure</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </remarks>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddResponseCompression();
            services.AddScoped<IDataTransportService, DataTransportService>();
            services.AddSingleton(Configuration);
            Environment.SetEnvironmentVariable(Constants.Environment.ENVIRONMENT_NAME_KEY, _hostingEnvironment.EnvironmentName.ToLower());
            services.AddMvc();
            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
            });
        }

        /// <summary>
        /// Configure service
        /// </summary>
        /// <param name="app">Application Builder</param>
        /// <param name="env">The environment</param>
        /// <param name="loggerFactory">Logger Factory</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </remarks>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseResponseCompression();
            app.UseValidationMiddleware();
            app.UseSecurityMiddleware();
            app.UseMvc();
        }
    }
}
