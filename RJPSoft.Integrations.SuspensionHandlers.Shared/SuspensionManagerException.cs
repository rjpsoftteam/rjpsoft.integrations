﻿using System;

namespace RJPSoft.Integrations.SuspensionHandlers
{
    /// <summary>
    /// Suspention Manager Exception
    /// </summary>
    public class SuspensionManagerException : Exception
    {
        /// <summary>
        /// Create a new instance of <see cref="SuspensionManagerException"/>
        /// </summary>
        public SuspensionManagerException() { }

        /// <summary>
        /// Create a new instance of <see cref="SuspensionManagerException"/>
        /// </summary>
        /// <param name="e">The exception</param>
        public SuspensionManagerException(Exception e)
            : base("SuspensionManager failed", e) { }
    }
}
