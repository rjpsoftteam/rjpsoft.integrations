﻿using RJPSoft.Integrations.Facebook;
using RJPSoft.Integrations.Facebook.EventArgs;
using RJPSoft.Integrations.Facebook.Model;
using System.Windows;

namespace Test_WPF_3._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FacebookIntegrationService _facebookService;

        public MainWindow()
        {
            InitializeComponent();
            _facebookService = FacebookIntegrationService.GetInstance("196078490889366");
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            var callback = FacebookIntegrationService.GetApplicationCallbackUri();
            var permissions = new FacebookPermissions[]
            {
                FacebookPermissions.PUBLIC_PROFILE,
                FacebookPermissions.USER_FRIENDS,
                FacebookPermissions.publish_actions,
                FacebookPermissions.user_photos
            };

            var loginUrl = _facebookService.GetLoginUri(callback.AbsoluteUri, permissions);
            _facebookService.LoginCompleted += _facebookService_LoginCompleted;
            _facebookService.LoginAsync(loginUrl);
        }

        private void _facebookService_LoginCompleted(object sender, FacebookLoginEventArgs e)
        {
            textBlockSuccess.Text = e.Success.ToString();
            textBlockStatusCode.Text = e.StatusCode.ToString();

            //if (e.Error != null)
            //{
            //    textBlockError.Text = e.Error.Error;
            //    textBlockErrorDescription.Text = e.Error.ErrorDescription;
            //    textBlockErrorReason.Text = e.Error.ErrorReason;
            //}
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            _facebookService.Logout();
        }

        private void userInfoButton_Click(object sender, RoutedEventArgs e)
        {
            _facebookService.GetUserInfoAsync();
        }
    }
}
