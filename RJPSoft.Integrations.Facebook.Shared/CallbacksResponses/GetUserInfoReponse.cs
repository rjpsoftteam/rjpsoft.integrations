﻿using RJPSoft.Integrations.Facebook.Model;
using System;

namespace RJPSoft.Integrations.Facebook.CallbacksResponses
{
    /// <summary>
    /// Argument for the GetUserInfoCompleted event
    /// </summary>
    public class GetUserInfoReponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetUserInfoReponse"/> class.
        /// </summary>
        /// <param name="responseStatusCode">The response status code.</param>
        /// <param name="user">The user.</param>
        /// <param name="recoveredFromFile">Indicates if this data was recovered from file</param>
        internal GetUserInfoReponse(ResponseStatusCode responseStatusCode, User user, bool recoveredFromFile)
        {
            User = user;
            ResponseStatusCode = responseStatusCode;
            RecoveredFromFile = recoveredFromFile;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GetUserInfoReponse"/> class.
        /// </summary>
        /// <param name="responseStatusCode">The response status code.</param>
        /// <param name="error">The error.</param>
        internal GetUserInfoReponse(ResponseStatusCode responseStatusCode, Exception error)
        {
            ResponseStatusCode = responseStatusCode;
            Error = error;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        public User User { get; }

        /// <summary>
        /// Gets the reponse status code.
        /// </summary>
        public ResponseStatusCode ResponseStatusCode { get; }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        public Exception Error { get; }

        /// <summary>
        /// Gets a value indicating whether this data was recovered from file.
        /// </summary>
        /// <value>
        /// <c>true</c> if this data was recovered from file; otherwise, <c>false</c>.
        /// </value>
        internal bool RecoveredFromFile { get; }
    }
}
