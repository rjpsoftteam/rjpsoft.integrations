﻿using RJPSoft.Integrations.Facebook.Model;

namespace RJPSoft.Integrations.Facebook.CallbacksResponses
{
    /// <summary>
    /// Argument for the LoginCompletedEvent
    /// </summary>
    public class FacebookLoginResponse
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookLoginResponse"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="statusCode">The status code.</param>
        /// <param name="error">The OAuth error.</param>
        public FacebookLoginResponse(bool success, ResponseStatusCode statusCode, FacebookOAuthResultError error = null)
        {
            Success = success;
            StatusCode = statusCode;
            Error = error;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the facebook OAuth error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public FacebookOAuthResultError Error { get; private set; }

        /// <summary>
        /// Gets the HTTP status code.
        /// </summary>
        /// <value>
        /// The HTTP status code.
        /// </value>
        public ResponseStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="FacebookLoginResponse"/> is success.
        /// </summary>
        /// <value>
        /// <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; private set; }

        #endregion Public Properties
    }
}