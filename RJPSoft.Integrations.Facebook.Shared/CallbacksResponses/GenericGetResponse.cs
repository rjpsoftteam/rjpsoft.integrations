﻿namespace RJPSoft.Integrations.Facebook.CallbacksResponses
{
    /// <summary>
    /// Hasndes the reponse for generic get requests
    /// </summary>
    public class GenericGetResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericGetResponse"/> class.
        /// </summary>
        /// <param name="data">The success result json data.</param>
        /// <param name="hasError">if set to <c>true</c> the GET operation resulted an error.</param>
        /// <param name="errorMessage">The error message.</param>
        public GenericGetResponse(string data, bool hasError, string errorMessage)
        {
            Data = data;
            HasError = hasError;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets The success result json data.
        /// </summary>
        public string Data { get; }

        /// <summary>
        /// Gets a value indicating whether t.he GET operation resulted an error
        /// </summary>
        /// <value>
        ///  <c>true</c> the GET operation resulted an error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError { get; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public string ErrorMessage { get; }
    }
}
