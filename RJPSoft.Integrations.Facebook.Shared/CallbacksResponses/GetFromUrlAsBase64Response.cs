﻿namespace RJPSoft.Integrations.Facebook.CallbacksResponses
{
    /// <summary>
    /// Holds the result from <see cref="FacebookIntegrationService.GetFromUrlAsBase64Async(string)"/>
    /// </summary>
    internal class GetFromUrlAsBase64Response
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GetFromUrlAsBase64Response"/> class.
        /// </summary>
        /// <param name="result">GET result.</param>
        /// <param name="error">GET error.</param>
        public GetFromUrlAsBase64Response(string result, string error = "")
        {
            Result = result;
            Error = error;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the GET error.
        /// </summary>
        public string Error { get; }

        /// <summary>
        /// Gets a value indicating whether the GET request has error.
        /// </summary>
        /// <value>
        /// <c>true</c> if has error; otherwise, <c>false</c>.
        /// </value>
        public bool HasError => !string.IsNullOrEmpty(Error);

        /// <summary>
        /// Gets the GET result.
        /// </summary>
        public string Result { get; }

        #endregion Public Properties
    }
}