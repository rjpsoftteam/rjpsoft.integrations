﻿using System.Collections.Generic;
using System.Linq;

namespace RJPSoft.Integrations.Facebook.ExtentionMethods
{
    /// <summary>
    /// Define String Extention Methods
    /// </summary>
    public static class StringExtentions
    {
        /// <summary>
        /// Concatenates the string in a comma separated string
        /// </summary>
        /// <param name="strings">The strings.</param>
        public static string ToCommaSparatedString(this IEnumerable<string> strings)
        {
            var result = string.Empty;
            if (strings != null)
            {
#if NETFX_CORE
                result = string.Join(",", strings);
#else
                result = string.Join(",", strings.ToArray());
#endif 
            }

            return result;
        }
    }
}
