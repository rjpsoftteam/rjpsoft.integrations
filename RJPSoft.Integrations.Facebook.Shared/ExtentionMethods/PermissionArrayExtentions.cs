﻿using RJPSoft.Integrations.Facebook.Model;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RJPSoft.Integrations.Facebook.ExtentionMethods
{
    /// <summary>
    /// FacebookPermissionsArray Extention Methods
    /// </summary>
    public static class PermissionArrayExtentions
    {
        /// <summary>
        /// Concatenates the permissionsto the correct format to send to facebook.
        /// </summary>
        /// <param name="permissions">The permissions to be concatenated</param>
        /// <returns>The permissions concatenated separated by comma</returns>
        public static string ConcatPermissions(this IEnumerable<FacebookPermissions> permissions)
        {
            var result = string.Empty;
            if (permissions != null)
            {
#if NETFX_CORE
                result = string.Join(",", permissions);
#else
                result = string.Join(",", permissions.ToStringArray());
#endif 
            }

            return result;
        }

        /// <summary>
        /// Convert the <see cref="IEnumerable{FacebookPermissions}"/> to a string array.
        /// </summary>
        /// <param name="permissions">The permissions</param>
        /// <returns>a  string[]</returns>
        public static string[] ToStringArray(this IEnumerable<FacebookPermissions> permissions) => permissions.Select(p => p.Permission).ToArray();

        /// <summary>
        /// Convert the <see cref="IEnumerable{FacebookPermissions}"/> to a string List.
        /// </summary>
        /// <param name="permissions">The permissions</param>
        /// <returns>a List<string></string></returns>
        public static List<string> ToStringList(this IEnumerable<FacebookPermissions> permissions) => permissions.Select(p => p.Permission).ToList();

        /// <summary>
        /// Verify if all the suplied permissions are READ type
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns><c>true</c> if all the permissions are READ type; otherwise <c>false</c></returns>
        public static bool AreAllPermissionReadType(this IEnumerable<FacebookPermissions> permissions) => !permissions.Any(p => p.IsWritePermission());

        /// <summary>
        /// Verify if all the suplied permissions are WRITE type
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns><c>true</c> if all the permissions are WRITE type; otherwise <c>false</c></returns>
        public static bool AreAllPermissionWriteType(this IEnumerable<FacebookPermissions> permissions) => !permissions.Any(p => p.IsReadPermission());

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="hashSet">The hash set.</param>
        /// <param name="values">The values.</param>
        public static void AddRange(this HashSet<string> hashSet, IEnumerable<string> values)
        {
            foreach (var value in values)
            {
                if(!string.IsNullOrEmpty(value))
                {
                    hashSet.Add(value);
                }
            }
        }
    }
}
