﻿using Facebook;

namespace RJPSoft.Integrations.Facebook.ExtentionMethods
{
    /// <summary>
    /// Contains extention methods for the <see cref="FacebookApiEventArgs"/>
    /// </summary>
    public static class FacebookApiEventArgsExtentions
    {

        /// <summary>
        /// Determines whether the call was successfull.
        /// </summary>
        /// <param name="e">The <see cref="FacebookApiEventArgs"/> instance containing the event data.</param>
        /// <returns>
        ///  <c>true</c> if the call was successfull; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSuccessfullCall(this FacebookApiEventArgs e) => e.Error == null && !e.Cancelled;

        /// <summary>
        /// Gets the stacktrace from the response.
        /// </summary>
        /// <param name="e">The <see cref="FacebookApiEventArgs"/> instance containing the event data.</param>
        /// <returns>The stacktrace from the call error or <see cref="string.Empty"/> if the call was successfull</returns>
        public static string GetStacktraceFromResponse(this FacebookApiEventArgs e) => e.Error != null ? e.Error.StackTrace : string.Empty;

        /// <summary>
        /// Gets the error message from the response.
        /// </summary>
        /// <param name="e">The <see cref="FacebookApiEventArgs"/> instance containing the event data.</param>
        /// <returns>The error message from the call error or <see cref="string.Empty"/> if the call was successfull</returns>
        public static string GetErrorMessageFromResponse(this FacebookApiEventArgs e) => e.Error != null ? e.Error.Message : string.Empty;
    }
}
