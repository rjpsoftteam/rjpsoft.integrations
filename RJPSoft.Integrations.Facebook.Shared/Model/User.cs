﻿using System;

namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Information about a user
    /// </summary>
    public class User
    {
        #region Internal Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="domainUser">The domain user.</param>
        internal User(Domain.User domainUser)
        {
            Id = domainUser.Id;
            FirstName = domainUser.FirstName;
            MiddleName = domainUser.MiddleName;
            LastName = domainUser.LastName;
            FullName = domainUser.FullName;
            Gender = domainUser.Gender;
            ProfilePictureBase64 = domainUser.ProfilePictureBase64;
        }

        #endregion Internal Constructors

        #region Public Properties

        /// <summary>
        /// Gets the first name.
        /// </summary>
        public string FirstName { get; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        public string FullName { get; }

        /// <summary>
        /// Gets or sets the user gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets the user Id.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the last name.
        /// </summary>
        public string LastName { get; }

        /// <summary>
        /// Gets themiddle name
        /// </summary>
        public string MiddleName { get; }

        /// <summary>
        /// Gets the profile picture base64.
        /// </summary>
        public string ProfilePictureBase64 { get; }

        /// <summary>
        /// Gets the profile picture byte array.
        /// </summary>
        /// <returns>The profile picture byte array</returns>
        public byte[] ProfilePictureByteArray => !string.IsNullOrEmpty(ProfilePictureBase64) ? Convert.FromBase64String(ProfilePictureBase64) : null;

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Equalses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns><c>true</c> if are the same object. <c>fase</c> otherwise</returns>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            User otehrUser = (User)obj;
            return (Id == otehrUser.Id);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"{FirstName}\n{MiddleName}\n{LastName}\n{FullName}\n{ProfilePictureBase64}";

        #endregion Public Methods
    }
}