﻿namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Represents a permission for your app request to the user authorize
    /// </summary>
    /// <remarks>
    /// see more: https://developers.facebook.com/docs/facebook-login/permissions/?locale=en_US
    /// </remarks>
    public class FacebookPermissions
    {
        #region Public Fields

        /// <summary>
        /// Provides the ability to both read and manage the ads for ad accounts you have access to. Please see Ads Management for details.
        /// </summary>
        public static readonly FacebookPermissions ADS_MANAGEMENT = new FacebookPermissions("ads_management", PermissionType.WRITE);

        /// <summary>
        /// Provides the access to Ads Insights API to pull ads report information for ad accounts you have access to.
        /// </summary>
        /// <remarks>
        /// This permission does not let you update purchase, update, or otherwise modify ads.
        /// </remarks>
        public static readonly FacebookPermissions ADS_READ = new FacebookPermissions("ads_read", PermissionType.READ);

        /// <summary>
        /// Read and write with Business Management API
        /// </summary>
        public static readonly FacebookPermissions BUSINESS_MANAGEMENT = new FacebookPermissions("business_management", PermissionType.WRITE);

        /// <summary>
        /// Provides access to the person's primary email address via the email property on the user object
        /// </summary>
        public static readonly FacebookPermissions EMAIL = new FacebookPermissions("email", PermissionType.READ);

        /// <summary>
        /// Enables your app to retrieve Page Access Tokens for the Pages and Apps that the person administrates.
        /// Apps need both manage_pages and publish_pages to be able to publish as a Page.
        /// </summary>
        public static readonly FacebookPermissions MANAGE_PAGES = new FacebookPermissions("manage_pages", PermissionType.READ);

        /// <summary>
        /// Provides the access to manage call to actions of the Pages that you manage.
        /// </summary>
        public static readonly FacebookPermissions PAGES_MANAGE_CTA = new FacebookPermissions("pages_manage_cta", PermissionType.WRITE);

        /// <summary>
        /// Lets your app manage Instant Articles on behalf of Facebook Pages administered by people using your app.
        /// </summary>
        public static readonly FacebookPermissions PAGES_MANAGE_INSTANT_ARTICLES = new FacebookPermissions("pages_manage_instant_articles", PermissionType.WRITE);

        /// <summary>
        /// This allows you to send and receive messages through a Facebook Page, but only within 24h hours after a user action.
        /// </summary>
        /// <remarks>
        /// For post 24h messages see <see cref="PAGES_MESSAGING_SUBSCRIPTIONS"/>. Conversations through this API can only begin when someone indicates—through 
        /// a Messenger plugin or directly messaging you—that they want to receive messages from you.
        /// </remarks>
        public static readonly FacebookPermissions PAGES_MESSAGING = new FacebookPermissions("pages_messaging", PermissionType.WRITE);

        /// <summary>
        /// This allows you to charge users in Messenger conversations on behalf of pages. 
        /// Intended for tangible goods only, not virtual or subscriptions.
        /// </summary>
        public static readonly FacebookPermissions PAGES_MESSAGING_PAYMENTS = new FacebookPermissions("pages_messaging_payments", PermissionType.WRITE);

        /// <summary>
        /// This allows you to send and receive messages through a Facebook Page. 
        /// This permission cannot be used to send promotional or advertising content
        /// </summary>
        /// <remarks>
        /// Conversations through this API can only begin when someone indicates—through a Messenger plugin 
        /// or directly messaging you—that they want to receive messages from you.
        /// </remarks>
        public static readonly FacebookPermissions PAGES_MESSAGING_PHONE_NUMBER = new FacebookPermissions("pages_messaging_phone_number", PermissionType.WRITE);

        /// <summary>
        /// This allows you to send and receive messages through a Facebook Page out of the 24h window opened by a user action. 
        /// This permission cannot be used to send promotional or advertising content.
        /// </summary>
        public static readonly FacebookPermissions PAGES_MESSAGING_SUBSCRIPTIONS = new FacebookPermissions("pages_messaging_subscriptions", PermissionType.WRITE);

        /// <summary>
        /// Provides the access to show the list of the Pages that you manage.
        /// </summary>
        public static readonly FacebookPermissions PAGES_SHOW_LIST = new FacebookPermissions("pages_show_list", PermissionType.READ);

        /// <summary>
        /// Provides access to a subset of items that are part of a person's public profile
        /// </summary>
        public static readonly FacebookPermissions PUBLIC_PROFILE = new FacebookPermissions("public_profile", PermissionType.READ);

        /// <summary>
        /// Provides access to publish Posts, Open Graph actions, achievements, scores and other activity on 
        /// behalf of a person using your app.
        /// </summary>
        /// <remarks>
        /// Because this permission lets you publish on behalf of a user please read the Platform Policies to 
        /// ensure you understand how to properly use this permission.
        /// </remarks>
        public static readonly FacebookPermissions PUBLISH_ACTIONS = new FacebookPermissions("publish_actions", PermissionType.WRITE);

        /// <summary>
        /// When you also have the manage_pages permission, gives your app the ability to post, 
        /// comment and like as any of the Pages managed by a person using your app.
        /// </summary>
        /// <remarks>
        /// Apps need both manage_pages and publish_pages to be able to publish as a Page.
        /// </remarks>
        public static readonly FacebookPermissions PUBLISH_PAGES = new FacebookPermissions("publish_pages", PermissionType.WRITE);

        /// <summary>
        /// Provides read-only access to the Audience Network Insights data for Apps the person owns.
        /// </summary>
        public static readonly FacebookPermissions READ_AUDIENCE_NETWORK_INSIGHTS = new FacebookPermissions("read_audience_network_insights", PermissionType.READ);

        /// <summary>
        /// Provides access to the names of custom lists a person has created to organize their friends. 
        /// This is useful for rendering an audience selector when someone is publishing stories to Facebook from your app.
        /// </summary>
        /// <remarks>
        /// This permission does not give access to a list of person's friends. If you want to access a person's friends 
        /// who also use your app, you should use the user_friends permission.
        /// </remarks>
        public static readonly FacebookPermissions READ_CUSTOM_FRIENDLISTS = new FacebookPermissions("read_custom_friendlists", PermissionType.WRITE);

        /// <summary>
        /// Provides read-only access to the Insights data for Pages, Apps and web domains the person owns.
        /// </summary>
        public static readonly FacebookPermissions READ_INSIGHTS = new FacebookPermissions("read_insights", PermissionType.READ);

        /// <summary>
        /// Provides the ability to read the messages in a person's Facebook Inbox through the inbox edge and the thread node.
        /// </summary>
        public static readonly FacebookPermissions READ_MAILBOXES = new FacebookPermissions("read_mailbox", PermissionType.READ);

        /// <summary>
        /// Provides the ability to read from the Page Inboxes of the Pages managed by a person. 
        /// This permission is often used alongside the manage_pages permission.
        /// </summary>
        public static readonly FacebookPermissions READ_PAGE_MAILBOXES = new FacebookPermissions("read_page_mailboxes", PermissionType.READ);

        /// <summary>
        /// Provides access to read the posts in a person's News Feed, or the posts on their Profile.
        /// </summary>
        public static readonly FacebookPermissions READ_STREAM = new FacebookPermissions("read_stream", PermissionType.READ);

        /// <summary>
        /// Provides the ability to read Instagram accounts you have access to. Please see <c>https://developers.facebook.com/docs/instagram-api/getting-started</c> 
        /// fot details.
        /// </summary>
        public static readonly FacebookPermissions INSTAGRAM_BASIC = new FacebookPermissions("instagram_basic", PermissionType.READ);

        /// <summary>
        /// Provides the ability to read Instagram accounts you have access to. Please see <c>https://developers.facebook.com/docs/instagram-api/getting-started</c> 
        /// fot details.
        /// </summary>
        public static readonly FacebookPermissions INSTAGRAM_MANAGE_COMMENTS = new FacebookPermissions("instagram_manage_comments", PermissionType.READ);

        /// <summary>
        /// Provides the ability to read insights of Instagram account you have access to. Please see <c>https://developers.facebook.com/docs/instagram-api/getting-started</c> 
        /// fot details.
        /// </summary>
        public static readonly FacebookPermissions INSTAGRAM_MANAGE_INSIGHTS = new FacebookPermissions("instagram_manage_insights", PermissionType.READ);

        /// <summary>
        /// Enables your app to read a person's notifications and mark them as read.
        /// This permission does not let you send notifications to a person.
        /// </summary>
        public static readonly FacebookPermissions MANAGE_NOTIFICATIONS = new FacebookPermissions("manage_notifications", PermissionType.WRITE);

        /// <summary>
        /// Provides the ability to set a person's attendee status on Facebook Events (e.g. attending, maybe, or declined).
        /// </summary>
        /// <remarks>
        /// This permission does not let you invite people to an event.
        /// This permission does not let you update an event's details.
        /// This permission does not let you create an event. There is no way to create an event through the API.
        /// </remarks>
        public static readonly FacebookPermissions RSVP_EVENT = new FacebookPermissions("rsvp_event", PermissionType.WRITE);

        /// <summary>
        /// Provides access to a person's personal description (the 'About Me' section on their Profile) through the bio property on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_ABOUT_ME = new FacebookPermissions("user_about_me", PermissionType.READ);

        /// <summary>
        /// Provides access to all common books actions published by any app the person has used. 
        /// This includes books they've read, want to read, rated or quoted.
        /// </summary>
        public static readonly FacebookPermissions USER_ACTIONS_BOOKS = new FacebookPermissions("user_actions.books", PermissionType.READ);

        /// <summary>
        /// Provides access to all common Open Graph fitness actions published by any app the person has used. 
        /// This includes runs, walks and bikes actions.
        /// </summary>
        public static readonly FacebookPermissions USER_ACTIONS_FITNESS = new FacebookPermissions("user_actions.fitness", PermissionType.READ);

        /// <summary>
        /// Provides access to all common Open Graph music actions published by any app the person has used. 
        /// This includes songs they've listened to, and playlists they've created.
        /// </summary>
        public static readonly FacebookPermissions USER_ACTIONS_MUSIC = new FacebookPermissions("user_actions.music", PermissionType.READ);

        /// <summary>
        /// Provides access to all common Open Graph news actions published by any app the person has used which publishes these actions. 
        /// This includes news articles they've read or news articles they've published.
        /// </summary>
        public static readonly FacebookPermissions USER_ACTIONS_NEWS = new FacebookPermissions("user_actions.news", PermissionType.READ);

        /// <summary>
        /// Provides access to all common Open Graph video actions published by any app the person has used which publishes these actions. 
        /// This includes videos they've watched, videos they've rated and videos they want to watch.
        /// </summary>
        public static readonly FacebookPermissions USER_ACTIONS_VIDEO = new FacebookPermissions("user_actions.video", PermissionType.READ);

        /// <summary>
        /// Access the date and month of a person's birthday. This may or may not include the person's year of birth,
        /// dependent upon their privacy settings and the access token being used to query this field.
        /// </summary>
        public static readonly FacebookPermissions USER_BIRTHDAY = new FacebookPermissions("user_birthday", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's education history through the education field on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_EDUCATION_HISTORY = new FacebookPermissions("user_education_history", PermissionType.READ);

        /// <summary>
        /// Provides read-only access to the Events a person is hosting or has RSVP'd to.
        /// </summary>
        public static readonly FacebookPermissions USER_EVENTS = new FacebookPermissions("user_events", PermissionType.READ);

        /// <summary>
        /// Provides access the list of friends that also use your app
        /// </summary>
        public static readonly FacebookPermissions USER_FRIENDS = new FacebookPermissions("user_friends", PermissionType.READ);

        /// <summary>
        /// Provides access to read a person's game activity (scores, achievements) in any game the person has played.
        /// </summary>
        public static readonly FacebookPermissions USER_GAMES_ACTIVITY = new FacebookPermissions("user_games_activity", PermissionType.READ);

        /// <summary>
        /// Enables your app to read the Groups a person is a member of through the groups edge on the User object.
        /// This permission does not allow you to create groups on behalf of a person. It is not possible to create groups via the Graph API.
        /// </summary>
        /// <remarks>
        /// This permission is only available for apps using Graph API version v2.3 or older.
        /// </remarks>
        public static readonly FacebookPermissions USER_GROUPS = new FacebookPermissions("user_groups", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's hometown location through the hometown field on the User object.
        /// This is set by the user on the Profile.
        /// </summary>
        public static readonly FacebookPermissions USER_HOMETOWN = new FacebookPermissions("user_hometown", PermissionType.READ);

        /// <summary>
        /// Provides access to the list of all Facebook Pages and Open Graph objects that a person has liked. 
        /// This list is available through the likes edge on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_LIKES = new FacebookPermissions("user_likes", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's current city through the location field on the User object. 
        /// The current city is set by a person on their Profile.
        /// </summary>
        public static readonly FacebookPermissions USER_LOCATION = new FacebookPermissions("user_location", PermissionType.READ);

        /// <summary>
        /// Lets your app read the content of groups a person is an admin of through the Groups edge on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_MANAGED_GROUPS = new FacebookPermissions("user_managed_groups", PermissionType.READ);

        /// <summary>
        /// Provides access to the photos a person has uploaded or been tagged in. This is available through the photos edge on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_PHOTOS = new FacebookPermissions("user_photos", PermissionType.READ);

        /// <summary>
        /// Provides access to the posts on a person's Timeline. Includes their own posts, posts they are tagged in, 
        /// and posts other people make on their Timeline.
        /// </summary>
        public static readonly FacebookPermissions USER_POSTS = new FacebookPermissions("user_posts", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's relationship interests as the interested_in field on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_RELATIONSHIP_DETAILS = new FacebookPermissions("user_relationship_details", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's relationship status, significant other and family members as fields on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_RELATIONSHIPS = new FacebookPermissions("user_relationships", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's religious and political affiliations.
        /// </summary>
        public static readonly FacebookPermissions USER_RELIGION_POLITICS = new FacebookPermissions("user_religion_politics", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's statuses. These are posts on Facebook which don't include links, videos or photos.
        /// </summary>
        /// <remarks>
        /// This permission is only available for apps using Graph API version v2.3 or older. If you are calling the endpoint /{user-id}/posts 
        /// or /{user-id}/feed, ask for the user_posts permission instead(only v2.3 or older).
        /// </remarks>
        public static readonly FacebookPermissions USER_STATUS = new FacebookPermissions("user_status", PermissionType.READ);

        /// <summary>
        /// Provides access to the Places a person has been tagged at in photos, videos, statuses and links.
        /// </summary>
        public static readonly FacebookPermissions USER_TAGGED_PLACES = new FacebookPermissions("user_tagged_places", PermissionType.READ);

        /// <summary>
        /// Provides access to the videos a person has uploaded or been tagged in.
        /// </summary>
        public static readonly FacebookPermissions USER_VIDEOS = new FacebookPermissions("user_videos", PermissionType.READ);

        /// <summary>
        /// Provides access to the person's personal website URL via the website field on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_WEBSITE = new FacebookPermissions("user_website", PermissionType.READ);

        /// <summary>
        /// Provides access to a person's work history and list of employers via the work field on the User object.
        /// </summary>
        public static readonly FacebookPermissions USER_WORK_HISTORY = new FacebookPermissions("user_work_history", PermissionType.READ);

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookPermissions"/> class.
        /// </summary>
        /// <param name="permission">The permission.</param>
        /// <param name="permissionType">The type of the <paramref name="permission"/>.</param>
        public FacebookPermissions(string permission, PermissionType permissionType)
        {
            Permission = permission;
            PermissionType = permissionType;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the facebook permission.
        /// </summary>
        public string Permission { get; }

        /// <summary>
        /// Gets the type of the permission.
        /// </summary>
        public PermissionType PermissionType { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Gets the permission that provides access to all of the person's custom Open Graph actions in the given app.
        /// </summary>
        /// <param name="appNamespace">The application namespace.</param>
        /// <returns>The permission that provides access to all of the person's custom Open Graph actions in the given app</returns>
        /// <exception cref="System.ArgumentNullException">appNamespace must not be null</exception>
        public FacebookPermissions GetPermissionForUserActionsInAApp(string appNamespace)
        {
            if (string.IsNullOrEmpty(appNamespace))
            {
                throw new System.ArgumentNullException(nameof(appNamespace));
            }

            return new FacebookPermissions($"user_actions:{appNamespace}", PermissionType.READ);
        }

        /// <summary>
        /// Determines whether this permission is a READ type.
        /// </summary>
        public bool IsReadPermission() => PermissionType == PermissionType.READ;

        /// <summary>
        /// Determines whether this permission is a WRITE type.
        /// </summary>
        public bool IsWritePermission() => PermissionType == PermissionType.WRITE;

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString() => Permission;

        #endregion Public Methods
    }
}