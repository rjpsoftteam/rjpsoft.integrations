﻿namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Represents Facebook permission types
    /// </summary>
    public enum PermissionType
    {
        /// <summary>
        /// Grants Read only permission
        /// </summary>
        READ,
        /// <summary>
        /// Grants Write permission
        /// </summary>
        WRITE
    }
}
