﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RJPSoft.Integrations.Facebook.Model
{
    public class FacebookApplication
    {
        private string _category;
        private string _link;
        private string _name;
        private string _id;

        internal FacebookApplication(Domain.FacebookApplication fbDomainApplication)
        {
            _category = fbDomainApplication.Category;
            _link = fbDomainApplication.Link;
            _name = fbDomainApplication.Name;
            _id = fbDomainApplication.Id;
        }

        public string Category => _category;

        public string Link => _link;

        public string Name => _name;

        public string Id => _id;
    }
}
