﻿using System;

namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Represents the status of a web request's response
    /// </summary>
    public enum ResponseStatusCode : short
    {
        /// <summary>
        /// The operation succeeded, and the response data is available.
        /// </summary>
        Success = 1,

        /// <summary>
        /// The operation was canceled by the user.
        /// </summary>
        UserCancel = 2,

        /// <summary>
        /// The operation failed because a specific HTTP error was returned, for example a 404
        /// </summary>
        ErrorHttp = 4,

        /// <summary>
        /// The operation failed because there is no internet connection
        /// </summary>
        NoInternet = 8,

        /// <summary>
        /// The operation failed for other reason rather than <see cref="ResponseStatusCode.UserCancel"/> or <see cref="ResponseStatusCode.ErrorHttp"/>
        /// </summary>
        GeneralError = 16,

        /// <summary>
        /// The user denied the oauth request
        /// </summary>
        UserDenied = 32
    }
}
