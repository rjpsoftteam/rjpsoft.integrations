﻿namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Detailed error result for facebook OAuth
    /// </summary>
    public class FacebookOAuthResultError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAuthResultError"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="description">The error description.</param>
        /// <param name="reason">The error reason.</param>
        public FacebookOAuthResultError(string error, string description, string reason)
        {
            Error = error;
            ErrorDescription = description;
            ErrorReason = reason;
        }

        /// <summary>
        /// Gets the Error that happens when using OAuth2 protocol.
        /// </summary>
        /// <remarks>
        /// See https://developers.facebook.com/docs/oauth/errors/ 
        /// </remarks>
        public string Error { get; }

        /// <summary>
        /// Gets the long error description for failed authentication if an error occurred.
        /// </summary>
        public string ErrorDescription { get; }

        /// <summary>
        /// Gets the short error reason for failed authentication if an error occurred.
        /// </summary>
        public string ErrorReason { get; }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString() => $"Error: {Error}\nError Description: {ErrorDescription}\nError Reason: {ErrorReason}";
    }
}
