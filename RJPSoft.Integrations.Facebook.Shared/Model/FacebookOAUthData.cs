﻿using Facebook;
using RJPSoft.Integrations.Facebook.Domain;
using System;
using UnityEngine;

namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Represents the data from facebook authentication
    /// </summary>
    public class FacebookOAUthData
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        /// <param name="token">Login token.</param>
        /// <param name="expirationDate">Login token expiration date.</param>
        internal FacebookOAUthData(string token, DateTime expirationDate)
        {
            AccessToken = token;
            Code = string.Empty;
            Expires = expirationDate;
            IsSuccess = true;
            State = string.Empty;
            LogContructorValues();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        /// <param name="facebookOauthResult">The facebook oauth result. <see cref="FacebookOAuthResult"/></param>
        internal FacebookOAUthData(FacebookOAuthResult facebookOauthResult)
        {
            AccessToken = facebookOauthResult.AccessToken;
            Code = facebookOauthResult.Code;
            Expires = facebookOauthResult.Expires.ToLocalTime();
            IsSuccess = facebookOauthResult.IsSuccess;
            State = facebookOauthResult.State;
            LogContructorValues();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        /// <param name="facebookOAUthPersistData">The facebook oauth result. <see cref="FacebookOAUthPersistData"/></param>
        internal FacebookOAUthData(FacebookOAUthPersistData facebookOAUthPersistData)
        {
            AccessToken = facebookOAUthPersistData.AccessToken;
            Code = facebookOAUthPersistData.Code;
            Expires = facebookOAUthPersistData.Expires.ToLocalTime();
            IsSuccess = facebookOAUthPersistData.IsSuccess;
            State = facebookOAUthPersistData.State;
            LogContructorValues();
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the access token.
        /// </summary>
        public string AccessToken { get; }

        /// <summary>
        /// Gets the code used to exchange with Facebook to retrieve access token.
        /// </summary>
        public string Code { get; }

        /// <summary>
        /// Gets the <see cref="System.DateTime"/> when the access token will expire.
        /// </summary>
        public DateTime Expires { get; }

        /// <summary>
        /// Gets a value indicating whether access token or code was successfully retrieved.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess { get; }

        /// <summary>
        /// Gets an opaque state used to maintain application state between the request and callback.
        /// </summary>
        public string State { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Determines whether [is token valid].
        /// </summary>
        /// <returns>
        /// <c>true</c> if [is token valid]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsTokenValid
        {
            get
            {
                var result = Expires >= DateTime.Now.AddMinutes(-5);
                Debug.Log($"[FacebookOAUthData][IsTokenValid] : {result}");
                return result;
            }
        }

        /// <summary>
        /// Determines whether [is user authenticated].
        /// </summary>
        /// <returns>
        /// <c>true</c> if [is user authenticated]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsUserAuthenticated
        {
            get
            {
                var result = AccessToken != null && IsTokenValid;
                Debug.Log($"[FacebookOAUthData][IsUserAuthenticated] : {result}");
                return result;
            }
        }

        #endregion Public Methods

        private void LogContructorValues()
        {
            Debug.Log($"[FacebookOAUthPersistData][CTOR] - AccessToken: {AccessToken}");
            Debug.Log($"[FacebookOAUthPersistData][CTOR] - code: {Code}");
            Debug.Log($"[FacebookOAUthPersistData][CTOR] - Expires(yyyy-mm-ddTHH24:mm:ss): {Expires.ToLocalTime().ToString("s")}");
            Debug.Log($"[FacebookOAUthPersistData][CTOR] - IsSuccess: {IsSuccess}");
            Debug.Log($"[FacebookOAUthPersistData][CTOR] - State: {State}");
        }
    }
}