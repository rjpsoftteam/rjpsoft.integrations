﻿namespace RJPSoft.Integrations.Facebook.Model
{
    /// <summary>
    /// Holds the Score Data
    /// </summary>
    public class Score
    {
        private ulong _score;
        private User _user;
        private FacebookApplication _facebookApplication;
        private int _position;
        private bool _isMe;

        /// <summary>
        /// Initializes a new instance of the <see cref="Score"/> class.
        /// </summary>
        /// <param name="domainScore">The domain score.</param>
        /// <param name="loggedUser">The logged user</param>
        public Score(Domain.Score domainScore, User loggedUser)
        {
            _score = domainScore.Value;
            _position = domainScore.Position;
            _facebookApplication = domainScore.FacebookApplication != null ? new FacebookApplication(domainScore.FacebookApplication) : null;
            _isMe = domainScore.User.Id == loggedUser?.Id;
            _user = _isMe ? loggedUser : domainScore.User != null ? new User(domainScore.User) : null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Score"/> class.
        /// </summary>
        public Score()
        {
        }

        /// <summary>
        /// Gets the score.
        /// </summary>
        public ulong Value => _score;

        /// <summary>
        /// Gets the user.
        /// </summary>
        public User User => _user;

        /// <summary>
        /// Gets the facebook application associated with the score.
        /// </summary>
        public FacebookApplication FacebookApplication => _facebookApplication;

        /// <summary>
        /// Gets or sets the score position when copared to others users.
        /// </summary>
        public int Position => _position;

        /// <summary>
        /// Gets a value indicating whether this the lgoed user score.
        /// </summary>
        /// <value>
        /// <c>true</c> if this the lgoed user score; otherwise, <c>false</c>.
        /// </value>
        public bool IsMe => _isMe;
    }
}
