﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.CallbacksResponses;
using RJPSoft.Integrations.Facebook.Model;
using RJPSoft.Security.Cryptography;
using System.Collections.Generic;
using System;
using UnityEngine;
using static RJPSoft.Integrations.Facebook.Constants;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Fields

        private GenericGetResponse _genericGetResponse;
        private Action<GenericGetResponse> _genericGetCompletedCallback;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Executes a GET operation over the specified <paramref name="path"/>.
        /// returns a json result via <paramref name="callback"/> parameter
        /// </summary>
        /// <param name="path">
        /// The path that will be called at the GET operation
        /// </param>
        /// <param name="useFacebookApi">
        /// <c>true</c> if the facebook graph api should be calles. 
        /// <c>false</c> is other service will be called
        /// </param>
        /// <param name="headerParams">
        /// Parameters that will be sent via header
        /// </param>
        /// <param name="queryParams">
        /// Paramenters that will be sent via query string
        /// </param>
        /// <param name="callback">The callback action that handles get user info response</param>
        /// <exception cref="ArgumentNullException"><paramref name="path"/> cannot be null or emptyz</exception>
        public void GenericGet(string path, bool useFacebookApi, Dictionary<string, object> queryParams,
            Dictionary<string, object> headerParams, Action<GenericGetResponse> callback)
        {
            ExecuteActionWithPreActions(() =>
            {
                if (string.IsNullOrEmpty(path))
                {
                    throw new ArgumentNullException((nameof(path)));
                }

                _genericGetCompletedCallback = callback;
                Debug.Log("[FacebookIntegrationService.Generic][Get] - Started");

                _client.GetCompleted += GenericGetFinished;
#pragma warning disable CS0618 // Type or member is obsolete
                _client.GetAsync(path, queryParams, Request.Get.GENERIC_GET_CALLBACK_KEY, headerParams, useFacebookApi);
#pragma warning restore CS0618 // Type or member is obsolete
                Debug.Log("[FacebookIntegrationService.Generic][GetUserInfoAsync] - GetUserInfo Requested, awaiting response");
            });
        }

        private void GenericGetFinished(object sender, FacebookApiEventArgs e)
        {
            if (e.UserState.Equals(Request.Get.GENERIC_GET_CALLBACK_KEY))
            {
                _client.GetCompleted -= GenericGetFinished;

                if (!e.Cancelled && e.Error == null)
                {
                    Debug.Log($"FacebookIntegrationService.Generic][GenericGetFinished] - Serialized info: {e.GetResultData().ToString()}");
                    _genericGetResponse = new GenericGetResponse(e.GetResultData().ToString(), false, string.Empty);

                }
                else if (e.Cancelled)
                {
                    _genericGetResponse = new GenericGetResponse(null, true, Request.REQUEST_CANCELED_ERRO_MESSAGE);
                    Debug.LogError($"FacebookIntegrationService.Generic][GenericGetFinished] - Get info error: {ResponseStatusCode.UserCancel}");
                }
                else
                {
                    _genericGetResponse = new GenericGetResponse(null, true, e.Error.Message);
                    Debug.LogError($"FacebookIntegrationService.Generic][GenericGetFinished] - Get info error: {ResponseStatusCode.GeneralError}");
                    Debug.LogException(e.Error);

                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void HandlesGenericFinished()
        {
            if (_genericGetResponse != null)
            {
                ActionHelpers.InvokeSafely(_genericGetCompletedCallback, _genericGetResponse, true);
                _genericGetResponse = null;
            }
        }

        #endregion Private Methods
    }
}