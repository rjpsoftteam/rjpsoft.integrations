﻿using System.Collections.Generic;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Holds contants values
    /// </summary>
    internal static class Constants
    {
        #region Internal Classes

        internal static class Security
        {
            internal const string iv = "gCc6jct8x+06xm9VIYmB3Q==";
            internal const string key = "5fB2t7G30TVAtsToh7USAGcw2P0HnUe6Ncu6U6rhcTA=";

            /// <summary>
            /// The public key to encrypt requests to server
            /// </summary>
            public const string PUBLIC_KEY = "BgIAAACkAABSU0ExAAQAAAEAAQABvLjFtkWZsVAlz0TlsZBUem3JKeL3VhXmgEFeDtojU5alWt2/gOOP5FTgtVgSA1vS3xGXzJph2zk8qIucrRFfb+IkaRirrckWfS9qpx0s8/XjB39Xot9OXdHzhsSXejF4Pn4TLRR2QlPIG6EsjPFcao1RJ7dCV7Cxi/bnYav2tQ==";
        }

        /// <summary>
        /// Holds constants used in request operations
        /// </summary>
        internal static class Request
        {
            internal const string REQUEST_CANCELED_ERRO_MESSAGE = "request canceled";

            /// <summary>
            /// Holds constants for GET operations
            /// </summary>
            internal static class Get
            {
                #region User Info

                /// <summary>
                /// Use string.Format to change {0} by the app_id
                /// </summary>
                internal const string FRIENDS_HIGHSCORES = "/{0}/scores";

                internal const int FRIENDS_HIGHSCORES_CALLBACK_KEY = 4;
                internal const string USER_HIGHSCORE = "/me/scores?fields=application,score";
                internal const int USER_HIGHSCORE_CALLBACK_KEY = 3;
                internal const int USER_INFO_CALLBACK_KEY = 1;
                internal const string USER_INFO_QUERY = "/me?fields=picture,first_name,middle_name,last_name,name";
                internal const int GENERIC_GET_CALLBACK_KEY = 5;
                internal const string DEBUG_TOKEN_QUERY = "/debug_token?fields=scopes&input_token=";
                internal const int DEBUG_TOKEN_CALLBACK_KEY = 6;

                #endregion User Info
            }

            /// <summary>
            /// Holds constants for POST operations
            /// </summary>
            internal static class Post
            {
                #region Internal Fields

                internal const string USER_HIGHSCORE = "/me/scores";
                internal const int USER_HIGHSCORE_CALLBACK_KEY = 2;

                #endregion Internal Fields
            }
        }

        /// <summary>
        /// Holds constants used for persistence
        /// </summary>
        internal static class Persistence
        {
            #region Internal Fields

            internal const string OAUTH_RESULT_STORE_KEY = "{39BEA0DE-1D26-4BBD-BC2F-0787BF76F0C5}";
            internal const string USER_INFO_STORE_KEY = "{9EBBD2DB-5041-45D5-AEDD-4430EB892833}";
            internal const string PERMISSONS_GRANTED_STORE_KEY = "{73399947-E174-42DB-BBAD-15409E93D629}";

            internal static readonly IEnumerable<string> ALL_KEYS = new List<string> { OAUTH_RESULT_STORE_KEY, USER_INFO_STORE_KEY };

            #endregion Internal Fields
        }

        #endregion Internal Classes
    }
}