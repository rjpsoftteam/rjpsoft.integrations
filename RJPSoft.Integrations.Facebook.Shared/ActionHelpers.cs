﻿using System;

/// <summary>
/// Extentions for Action class
/// </summary>
public static class ActionHelpers
{
    /// <summary>
    /// Invokes the Action safely.
    /// </summary>
    /// <typeparam name="T">Parameter Type</typeparam>
    /// <param name="action">The action to be executed.</param>
    /// <param name="paramenter">The argument that will be passed to the action.</param>
    /// <param name="setActionToNull">If <c>true</c> sets the action to null after invoking it. If <c>false</c> do nothing</param>
    public static void InvokeSafely<T>(this Action<T> action, T paramenter, bool setActionToNull = false)
    {
        try
        {
            action?.Invoke(paramenter);
        }
        finally
        {
            if (setActionToNull)
            {
                action = null;
            }
        }
    }

    /// <summary>
    /// Invokes the Action safely.
    /// </summary>
    /// <typeparam name="T1">First Parameter Type</typeparam>
    /// <typeparam name="T2">Second Parameter Type</typeparam>
    /// <param name="action">The action to be executed.</param>
    /// <param name="paramenter">The first argument that will be passed to the action.</param>
    /// <param name="parameter2">The second argument that will be passed to the action.</param>
    /// <param name="setActionToNull">If <c>true</c> sets the action to null after invoking it. If <c>false</c> do nothing</param>
    public static void InvokeSafely<T1,T2>(this Action<T1,T2> action, T1 paramenter, T2 parameter2, bool setActionToNull = false)
    {
        try
        {
            action?.Invoke(paramenter, parameter2);
        }
        finally
        {
            if (setActionToNull)
            {
                action = null;
            }
        }
    }

    /// <summary>
    /// Invokes the Action safely.
    /// </summary>
    /// <param name="action">The action to be executed.</param>
    /// <param name="setActionToNull">If <c>true</c> sets the action to null after invoking it. If <c>false</c> do nothing</param>
    public static void InvokeSafely(this Action action, bool setActionToNull = false)
    {
        try
        {
            action?.Invoke();
        }
        finally
        {
            if (setActionToNull)
            {
                action = null;
            }
        }
    }
}