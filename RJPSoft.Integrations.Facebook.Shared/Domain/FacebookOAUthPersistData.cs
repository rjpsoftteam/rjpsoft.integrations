﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.Model;
using System;
using UnityEngine;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Represents the data from facebook authentication
    /// </summary>
    internal class FacebookOAUthPersistData
    {
        [JsonProperty("access_token")]
        private string _accessToken;

        [JsonProperty("code")]
        private string _code;

        [JsonProperty("expires")]
        private DateTime _expires;

        [JsonProperty("is_success")]
        private bool _isSuccess;

        [JsonProperty("state")]
        private string _state;

        #region public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        public FacebookOAUthPersistData()
        {
            //empty for serialization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        /// <param name="facebookOauthResult">The facebook oauth result. <see cref="FacebookOAuthResult"/></param>
        public FacebookOAUthPersistData(FacebookOAuthResult facebookOauthResult)
        {
            _accessToken = facebookOauthResult.AccessToken;
            _code = facebookOauthResult.Code;
            _expires = facebookOauthResult.Expires.ToLocalTime();
            _isSuccess = facebookOauthResult.IsSuccess;
            _state = facebookOauthResult.State;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAUthData"/> class.
        /// </summary>
        /// <param name="facebookOAUthData">The facebook oauth result. <see cref="FacebookOAUthData"/></param>
        public FacebookOAUthPersistData(FacebookOAUthData facebookOAUthData)
        {
            _accessToken = facebookOAUthData.AccessToken;
            _code = facebookOAUthData.Code;
            _expires = facebookOAUthData.Expires.ToLocalTime();
            _isSuccess = facebookOAUthData.IsSuccess;
            _state = facebookOAUthData.State;
        }

        #endregion public Constructors

        #region public Properties

        /// <summary>
        /// Gets the access token.
        /// </summary>
        public string AccessToken => _accessToken;

        /// <summary>
        /// Gets the code used to exchange with Facebook to retrieve access token.
        /// </summary>
        public string Code => _code;

        /// <summary>
        /// Gets the <see cref="System.DateTime"/> when the access token will expire.
        /// </summary>
        public DateTime Expires => _expires;

        /// <summary>
        /// Gets a value indicating whether access token or code was successfully retrieved.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess => _isSuccess;

        /// <summary>
        /// Determines whether [is token valid].
        /// </summary>
        /// <returns>
        /// <c>true</c> if [is token valid]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsTokenValid => Expires >= DateTime.Now.AddMinutes(-5);

        /// <summary>
        /// Determines whether [is user authenticated].
        /// </summary>
        /// <returns>
        /// <c>true</c> if [is user authenticated]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsUserAuthenticated => AccessToken != null && IsTokenValid;

        /// <summary>
        /// Gets an opaque state used to maintain application state between the request and callback.
        /// </summary>
        public string State => _state;

        #endregion public Properties
    }
}