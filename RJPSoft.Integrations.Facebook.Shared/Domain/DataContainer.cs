﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Holds a generic object that maps to a json property array called "data"
    /// </summary>
    /// <typeparam name="T">Type of the "data"</typeparam>
    public class DataContainer<T>
    {
        [JsonProperty("data")]
        private T _data = default(T);

        /// <summary>
        /// Gets the data.
        /// </summary>
        public T Data => _data;
    }
}
