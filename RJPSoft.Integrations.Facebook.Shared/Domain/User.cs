﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Information about a user
    /// </summary>
    public class User
    {
        #region Private Fields

        [JsonProperty("picture")]
        private DataContainer<Picture> _profilePictureUrl = null;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="modelUser">The model user.</param>
        public User(Model.User modelUser)
        {
            FirstName = modelUser.FirstName;
            MiddleName = modelUser.MiddleName;
            LastName = modelUser.LastName;
            FullName = modelUser.FullName;
            Gender = modelUser.Gender;
            Id = modelUser.Id;
            ProfilePictureBase64 = modelUser.ProfilePictureBase64;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
            //empty for serializer
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        [JsonProperty("name")]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the user gender.
        /// </summary>
        [JsonProperty("gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [JsonProperty("last_name")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the name of the middle.
        /// </summary>
        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the profile picture encode as base64.
        /// </summary>
        [JsonProperty("profile_picture_base_64")]
        public string ProfilePictureBase64 { get; set; }

        /// <summary>
        /// Gets the profile picture URL.
        /// </summary>
        [JsonIgnore]
        public string ProfilePictureUrl => _profilePictureUrl?.Data.Url;

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Equalses the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns><c>true</c> if are the same object. <c>fase</c> otherwise</returns>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var otehrUser = (User)obj;
            return (Id == otehrUser.Id);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Retuns a string that represents this instance
        /// </summary>
        /// <returns>A string</returns>
        public override string ToString() => $"{FullName} \n {ProfilePictureUrl}";

        #endregion Public Methods
    }
}