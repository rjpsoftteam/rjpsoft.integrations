﻿using Newtonsoft.Json;
using System;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Holds the Score Data
    /// </summary>
    [JsonObject]
    public class Score : IComparable
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the position when copared to others users.
        /// </summary>
        [JsonProperty("position")]
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the user who has the score.
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the score value.
        /// </summary>
        [JsonProperty("score")]
        public ulong Value { get; set; }

        /// <summary>
        /// Gets or sets the facebook application associated with the score.
        /// </summary>
        [JsonProperty("application")]
        public FacebookApplication FacebookApplication { get; set; }


        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" /> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in the sort order.
        /// </returns>
        public int CompareTo(object obj)
        {
            if (!(obj is Score))
            {
                return -1;
            }

            return Value.CompareTo(((Score)obj).Value);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (Score)obj;
            return other.User != null && other.User.Id == User.Id;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode() => User.Id.GetHashCode();

        #endregion Public Methods
    }
}
