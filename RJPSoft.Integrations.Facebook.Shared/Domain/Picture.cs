﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Holds the picture data information
    /// </summary>
    public class Picture
    {
        /// <summary>
        /// Gets or sets the picture URL.
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
