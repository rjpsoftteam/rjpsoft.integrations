﻿using Newtonsoft.Json;

namespace RJPSoft.Integrations.Facebook.Domain
{
    public class FacebookApplication
    {
        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
