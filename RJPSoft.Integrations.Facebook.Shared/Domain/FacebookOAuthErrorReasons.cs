﻿using System;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Represents the Possible OAuth Error Reasons
    /// </summary>
    public enum FacebookOAuthErrorReasons
    {
        /// <summary>
        /// The user denied the oauth request
        /// </summary>
        user_denied = 1,
        //next = 2,
        //next = 4
    }
}
