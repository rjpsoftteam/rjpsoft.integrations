﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RJPSoft.Integrations.Facebook.Domain
{
    /// <summary>
    /// Holds information about the permisisons granted
    /// </summary>
    internal class PermissionsData
    {
        /// <summary>
        /// Contains all the permissions granted
        /// </summary>
        [JsonProperty("scopes")]
        public List<string> Scopes { internal set; get; } = new List<string>();
    }
}
