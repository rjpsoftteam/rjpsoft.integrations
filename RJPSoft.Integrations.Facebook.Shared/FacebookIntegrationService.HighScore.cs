﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.ExtentionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static RJPSoft.Integrations.Facebook.Constants;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Fields

        private Action<bool> _postUserHighScoreCallback;
        private Action<Model.Score> _getLogedUserHighScoreCallback;
        private bool _postUserHighScoreCompleted;
        private bool _postWasSuccessfull;
        private bool _getHighScoreCompleted;
        private Model.Score _getHighScoreResponse;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Posts the user high score.
        /// </summary>
        /// <param name="highScore">The high score to be posted.</param>
        /// <param name="path">
        /// The path to get the user info. If no one provided, facebook graph api will be called with 
        /// /me?fields=picture,first_name,middle_name,last_name,name and <paramref name="useFacebookApi"/> will be setted to true
        /// </param>
        /// <param name="useFacebookApi">
        /// <c>true</c> if the facebook graph api should be called. <c>false</c> if other service will be called
        /// </param>
        /// <param name="headerParams">Parameters that will be sent via header</param>
        /// <param name="postUserHighScoreCallback">
        /// The callback that will be called when posts finish. 
        /// The boolean paramater indicates if the post was successfull
        /// </param>
        public void PostUserHighScore(ulong highScore, string path, bool useFacebookApi, Dictionary<string, object> headerParams, 
            Action<bool> postUserHighScoreCallback)
        {
            ExecuteActionWithPreActions(() =>
            {
                Debug.Log($"[FacebookIntegrationService][PostUserHighScore] - highscore to send: {highScore}");

                _postUserHighScoreCallback = postUserHighScoreCallback;
                _client.PostCompleted += PostUserHighScoreCompleted;

                if (string.IsNullOrEmpty(path))
                {
                    path = Request.Post.USER_HIGHSCORE;
                    useFacebookApi = true;
                }

                var bodyParams = new Dictionary<string, object> { {"score", highScore } };

#pragma warning disable CS0618 // Type or member is obsolete
                _client.PostAsync(path, bodyParams, Request.Post.USER_HIGHSCORE_CALLBACK_KEY, headerParams, useFacebookApi);
#pragma warning restore CS0618 // Type or member is obsolete
            });
        }

        /// <summary>
        /// Gets the loged user high score.
        /// </summary>
        /// <param name="getLogedUserHighScoreCallback">The callbak that will be called when the get operation finishes.</param>
        public void GetLogedUserHighScore(Action<Model.Score> getLogedUserHighScoreCallback)
        {
            ExecuteActionWithPreActions(() =>
            {
                _getLogedUserHighScoreCallback = getLogedUserHighScoreCallback;
                _client.GetCompleted += GetLogedUserHighScoreCompleted;

                Debug.Log("[FacebookIntegrationService][GetLogedUserHighScore] - Getting highscore");

#pragma warning disable CS0618 // Type or member is obsolete
                _client.GetAsync(Request.Get.USER_HIGHSCORE, null, Request.Get.USER_HIGHSCORE_CALLBACK_KEY, null);
#pragma warning restore CS0618 // Type or member is obsolete
            });
        }

        #endregion Public Methods

        #region Private Methods

        private void GetLogedUserHighScoreCompleted(object sender, FacebookApiEventArgs e)
        {
            if (e.UserState.Equals(Request.Get.USER_HIGHSCORE_CALLBACK_KEY))
            {
                _client.GetCompleted -= GetLogedUserHighScoreCompleted;
                Debug.Log($"[FacebookIntegrationService][GetLogedUserHighScoreCompleted] - response:\n{e.GetResultData().ToString()}");

                if (e.IsSuccessfullCall())
                {
                    var response = JsonConvert.DeserializeObject<Domain.DataContainer<Domain.Score[]>>(e.GetResultData().ToString());
                    if (response.Data.Any())
                    {
                        _getHighScoreResponse = new Model.Score(response.Data.Where(s => s.FacebookApplication.Id == AppId).FirstOrDefault(), LogedUser);
                    }
                    else
                    {
                        _getHighScoreResponse = new Model.Score();
                    }
                }
                else
                {
                    LogResponseStatus(e, nameof(FacebookIntegrationService.GetLogedUserHighScoreCompleted));
                    _getHighScoreResponse = null;

                }

                _getHighScoreCompleted = true;
            }
        }

        private void HandleGetLogedUserHighScoreCompleted()
        {
            if (_getHighScoreCompleted)
            {
                _getHighScoreCompleted = false;
                ActionHelpers.InvokeSafely(_getLogedUserHighScoreCallback, _getHighScoreResponse, true);
            }
        }

        private void HandlePostUserHighScoreCompleted()
        {
            if (_postUserHighScoreCompleted)
            {
                _postUserHighScoreCompleted = false;
                ActionHelpers.InvokeSafely(_postUserHighScoreCallback, _postWasSuccessfull, true);
            }
        }

        private void PostUserHighScoreCompleted(object sender, FacebookApiEventArgs e)
        {
            if (e.UserState.Equals(Request.Post.USER_HIGHSCORE_CALLBACK_KEY))
            {
                _client.PostCompleted -= PostUserHighScoreCompleted;
                _postWasSuccessfull = e.IsSuccessfullCall();
                LogResponseStatus(e, nameof(FacebookIntegrationService.PostUserHighScoreCompleted));
                _postUserHighScoreCompleted = true;
            }
        }

        private void LogResponseStatus(FacebookApiEventArgs e, string methodName)
        {
            bool isSuccessfullCall = e.IsSuccessfullCall();
            if (!isSuccessfullCall)
            {
                string errorMessage = e.GetErrorMessageFromResponse();
                string stacktrace = e.GetStacktraceFromResponse();
                Debug.Log($"[FacebookIntegrationService][{methodName}] - successffuly: {isSuccessfullCall}");
                Debug.Log($"[FacebookIntegrationService][{methodName}] - error message: {errorMessage}");
                Debug.Log($"[FacebookIntegrationService][{methodName}] - error stacktrace: {stacktrace}");
                Debug.Log($"[FacebookIntegrationService][{methodName}] - canceled: {e.Cancelled}"); 
            }
        }

        #endregion Private Methods
    }
}