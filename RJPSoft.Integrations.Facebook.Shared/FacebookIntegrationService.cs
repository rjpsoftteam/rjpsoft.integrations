﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.CallbacksResponses;
using RJPSoft.Security.Cryptography;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static RJPSoft.Integrations.Facebook.Constants;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Fields

        private static string _appId;
        private static string _informedLoginToken;
        private static DateTime? _informedTokenExpirationDate;
        private static Action<bool> _onHideUnity;

        private FacebookClient _client;

        #endregion Private Fields

        #region Internal Properties

        /// <summary>
        /// Gets or sets the callback for the method <see cref="GetFromUrlAsBase64Async(string)"/>.
        /// </summary>
        internal Action<GetFromUrlAsBase64Response> GetFromUrlAsBase64Callback { get; set; }

        #endregion Internal Properties

        #region Public Properties

        /// <summary>
        /// Indicates Is this instance is initialized
        /// </summary>
        public static bool IsInitialied { get; private set; }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public string AccessToken
        {
            get { return _client.AccessToken; }
            private set { _client.AccessToken = value; }
        }

        /// <summary>
        /// Gets the access token expiration date.
        /// </summary>
        public DateTime AccessTokenExpirationDate => _oauthData.Expires;

        /// <summary>
        /// Gets or sets the AppID.
        /// </summary>
        public string AppId
        {
            get { return _client.AppId; }
            set { _client.AppId = value; }
        }

        /// <summary>
        /// Gets the loged user.
        /// </summary>
        public Model.User LogedUser { get; private set; }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes <see cref="FacebookIntegrationService"/> class. use <see cref="Singleton{FacebookIntegrationService}.Instance"/> to get a instance
        /// </summary>
        /// <param name="initializationFinishedCallback">Callback that will be called when the initialization finishes</param>
        /// <param name="appId">The Facebook application identifier.</param>
        /// <param name="onHideUnity">Callback called when the system leaves the app to login to facebook</param>
        /// <param name="loginToken">The facebook login token, used for Android and iOS</param>
        /// <param name="tokenExpirationDate">The login token expirationDate. Used for Android and iOS</param>
        /// <remarks>
        /// see http://developers.facebook.com/docs/apps/register
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="appId"/> must not be null</exception>
        public static void Initialize(Action initializationFinishedCallback, string appId, Action<bool> onHideUnity, string loginToken = "",
            DateTime? tokenExpirationDate = null)
        {
            if (string.IsNullOrEmpty(appId))
            {
                throw new ArgumentNullException(nameof(appId));
            }

            _onHideUnity = onHideUnity;

            CheckTokenInitializationParameters(loginToken, tokenExpirationDate);

            Debug.Log($"[FacebookIntegrationService][Initialize] - app id {appId}");
            Debug.Log($"[FacebookIntegrationService][Initialize] - login Token {loginToken}");
            Debug.Log($"[FacebookIntegrationService][Initialize] - token Expiration Date {tokenExpirationDate}");

            _appId = appId;
            _informedLoginToken = loginToken;
            _informedTokenExpirationDate = tokenExpirationDate;

            //Instantiate the singleton
            Instance = Instance;

            // When creating a new instance (Instance = Instance) the Awake method will be called, and the instantiation will only finish after 
            // the Awake method finishes, so the initialization is over when the instantiation is over.
            Debug.Log("[FacebookIntegrationService][InitFacebookIntegrationService] - Inicialized");
            IsInitialied = true;
            ActionHelpers.InvokeSafely(initializationFinishedCallback, true);
        }


        /// <summary>
        /// Executes the required action before other actions.
        /// 1th - Verify if the user is loged in
        /// </summary>
        /// <param name="action">The action to be executed</param>
        /// <exception cref="System.InvalidOperationException">The user must be loged in to realize this operation</exception>
        public void ExecuteActionWithPreActions(Action action)
        {
            if (IsUserAuthenticated)
            {
                ActionHelpers.InvokeSafely(action);
            }
            else
            {
                throw new InvalidOperationException("The user must be loged in to realize this operation");
            }
        }


        #endregion Public Methods

        #region Private Methods

        private static void CheckTokenInitializationParameters(string loginToken, DateTime? tokenExpirationDate)
        {
            if (string.IsNullOrEmpty(loginToken) && tokenExpirationDate != null)
            {
                throw new ArgumentNullException(nameof(loginToken), $"if {nameof(loginToken)} is informed, so must be {nameof(tokenExpirationDate)}");
            }
            else if (!string.IsNullOrEmpty(loginToken) && tokenExpirationDate == null)
            {
                throw new ArgumentNullException(nameof(tokenExpirationDate), $"if {nameof(tokenExpirationDate)} is informed, so must be {nameof(loginToken)}");
            }
        }

        private static string GetSavedDecryptedValue(string key, string defaultValue = "")
        {
            var result = defaultValue;

            if (PlayerPrefs.HasKey(key))
            {
                var encryptedString = PlayerPrefs.GetString(key, defaultValue);

                if (!string.IsNullOrEmpty(encryptedString))
                {
                    result = CryptoHandler.DecryptAES(encryptedString, Constants.Security.key, Constants.Security.iv);
                }
            }

            return result;
        }

        /// <summary>
        /// Called when an instance is created
        /// </summary>
        private void Awake()
        {
            InitFacebookIntegrationService();
        }

        private object Deserializer(string value, Type type)
        {
            string typeString = type != null ? type.Name : "object";
            Debug.Log($"[FacebookIntegrationService][Deserializer] - string to deserialize: {value}");
            Debug.Log($"[FacebookIntegrationService][Deserializer] - type to deserialize: {typeString}");

            return JsonConvert.DeserializeObject(value, type);
        }

        private IEnumerator GetFromUrlAsBase64Async(string url)
        {
            var www = new WWW(url);

            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                Debug.Log("[FacebookIntegrationService][GetFromUrlAsBase64Async] - download OK");
                ActionHelpers.InvokeSafely(GetFromUrlAsBase64Callback, new GetFromUrlAsBase64Response(Convert.ToBase64String(www.bytes)));
            }
            else
            {
                Debug.LogError($"[FacebookIntegrationService][GetFromUrlAsBase64Async] - download error: {www.error}");
                ActionHelpers.InvokeSafely(GetFromUrlAsBase64Callback, new GetFromUrlAsBase64Response(null, www.error));
            }
        }
        private void InitFacebookIntegrationService()
        {
            GetOAUthResultFromFile();

            if (_client == null)
            {
                _client = new FacebookClient();
            }

            InitializeFacebookClient();
            InitializeLogedUser();
        }

        private void InitializeFacebookClient()
        {
            _client.AppId = _appId;

            if (_oauthData != null)
            {
                _client.AccessToken = _oauthData.AccessToken;
            }
            else if (!string.IsNullOrEmpty(_informedLoginToken))
            {
                _client.AccessToken = _informedLoginToken;
                _oauthData = new Model.FacebookOAUthData(_informedLoginToken, _informedTokenExpirationDate.Value);
                PersistOAUthResult(new Domain.FacebookOAUthPersistData(_oauthData));
            }

            _client.SetJsonSerializers(JsonConvert.SerializeObject, Deserializer);
        }

        private void InitializeLogedUser()
        {
            var user = GetSavedDecryptedValue(Persistence.USER_INFO_STORE_KEY);
            if (!string.IsNullOrEmpty(user))
            {
                Debug.Log($"[FacebookIntegrationService][InitializeLogedUser] - recovered loged user: {user}");
                LogedUser = new Model.User(JsonConvert.DeserializeObject<Domain.User>(user));
                InitializePermissionsGranted();
            }
        }

        private void InitializePermissionsGranted()
        {
            var permissions = GetSavedDecryptedValue(Persistence.PERMISSONS_GRANTED_STORE_KEY);
            if (!string.IsNullOrEmpty(permissions))
            {
                Debug.Log($"[FacebookIntegrationService][InitializePermissionsGranted] - recovered permissions: {permissions}");
                Permissions = new HashSet<string>(JsonConvert.DeserializeObject<Domain.PermissionsData>(permissions).Scopes);
            }
        }
        private void Update()
        {
            HandlesLoginFinished();
            HandlesGetUserInfoFinished();
            HandlePostUserHighScoreCompleted();
            HandleGetLogedUserHighScoreCompleted();
            HandlesGenericFinished();
        }

        #endregion Private Methods
    }
}