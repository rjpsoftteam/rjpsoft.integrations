﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.CallbacksResponses;
using RJPSoft.Integrations.Facebook.Model;
using RJPSoft.Security.Cryptography;
using System.Collections.Generic;
using System;
using UnityEngine;
using static RJPSoft.Integrations.Facebook.Constants;

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Fields

        private GetUserInfoReponse _getUserInfoResponse;
        private Action<GetUserInfoReponse> _getUserInfoCompletedCallback;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Gets the user information.
        /// returns a <see cref="Model.User"/> via <paramref name="callback"/> parameter/>/>
        /// </summary>
        /// <param name="path">
        /// The path to get the user info. If no one provided, facebook graph api will be called with 
        /// /me?fields=picture,first_name,middle_name,last_name,name and <paramref name="useFacebookApi"/> will be setted to true
        /// </param>
        /// <param name="useFacebookApi">
        /// <c>true</c> if the facebook graph api should be calles. 
        /// <c>false</c> is other service will be called
        /// </param>
        /// <param name="headerParams">
        /// Parameters that will be sent via header
        /// </param>
        /// <param name="queryParams">
        /// Paramenters that will be sent via query string
        /// </param>
        /// <param name="callback">The callback action that handles get user info response</param>
        public void GetLogedUserInfo(string path, bool useFacebookApi, Dictionary<string, object> queryParams, 
            Dictionary<string, object> headerParams, Action<GetUserInfoReponse> callback)
        {
            ExecuteActionWithPreActions(() =>
            {
                _getUserInfoCompletedCallback = callback;
                Debug.Log("[FacebookIntegrationService][GetUserInfoAsync] - GetUserInfo Started");

                var persistedUser = GetPersistedUserData();
                Debug.Log($"[FacebookIntegrationService][GetUserInfoAsync] - Persisted User Data: {persistedUser}");

                if (persistedUser == null)
                {
                    Debug.Log("[FacebookIntegrationService][GetUserInfoAsync] - No user data persisted, starting request to facebook");
                    if(string.IsNullOrEmpty(path))
                    {
                        path = Request.Get.USER_INFO_QUERY;
                        useFacebookApi = true;
                    }

                    _client.GetCompleted += GetUserInfoFinished;
#pragma warning disable CS0618 // Type or member is obsolete
                    _client.GetAsync(path, queryParams, Request.Get.USER_INFO_CALLBACK_KEY, headerParams, useFacebookApi);
#pragma warning restore CS0618 // Type or member is obsolete
                    Debug.Log("[FacebookIntegrationService][GetUserInfoAsync] - GetUserInfo Requested, awaiting response");
                }
                else
                {
                    Debug.Log("[FacebookIntegrationService][GetUserInfoAsync] - user data Recovered from disk");
                    _getUserInfoResponse = new GetUserInfoReponse(ResponseStatusCode.Success, new Model.User(persistedUser), true);
                }
            });
        }

        private void GetUserInfoFinished(object sender, FacebookApiEventArgs e)
        {
            if (e.UserState.Equals(Request.Get.USER_INFO_CALLBACK_KEY))
            {
                _client.GetCompleted -= GetUserInfoFinished;

                if (!e.Cancelled && e.Error == null)
                {
                    Debug.Log($"FacebookIntegrationService][GetUserInfoFinished] - Serialized user info: {e.GetResultData().ToString()}");
                    var user = JsonConvert.DeserializeObject<Domain.User>(e.GetResultData().ToString());
                    _getUserInfoResponse = new GetUserInfoReponse(ResponseStatusCode.Success, new User(user), false);

                }
                else if (e.Cancelled)
                {
                    _getUserInfoResponse = new GetUserInfoReponse(ResponseStatusCode.UserCancel, new Exception("Cancel Requested"));
                    Debug.LogError($"FacebookIntegrationService][GetUserInfoFinished] - Get user info error: {ResponseStatusCode.UserCancel}");
                }
                else
                {
                    _getUserInfoResponse = new GetUserInfoReponse(ResponseStatusCode.GeneralError, e.Error);
                    Debug.LogError($"FacebookIntegrationService][GetUserInfoFinished] - Get user info error: {ResponseStatusCode.GeneralError}");
                    Debug.LogException(e.Error);

                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        private Domain.User GetPersistedUserData()
        {
            Domain.User result = null;
            var decryptedValue = GetSavedDecryptedValue(Persistence.USER_INFO_STORE_KEY);
            Debug.Log($"[FacebookIntegrationService][GetPersistedUserData] - Saved user data: {decryptedValue}");

            if (!string.IsNullOrEmpty(decryptedValue))
            {
                result = JsonConvert.DeserializeObject<Domain.User>(decryptedValue);
            }

            return result;
        }
        
        private void HandlesGetUserInfoFinished()
        {
            if (_getUserInfoResponse != null)
            {
                if (!_getUserInfoResponse.RecoveredFromFile && _getUserInfoResponse.ResponseStatusCode == ResponseStatusCode.Success)
                {
                    var serializedResult = JsonConvert.SerializeObject(new Domain.User(_getUserInfoResponse.User));
                    Debug.Log($"[FacebookIntegrationService][HandlesGetUserInfoFinished] - Saving user data: {serializedResult}");
                    var securedString = CryptoHandler.EncryptAES(serializedResult, Constants.Security.key, Constants.Security.iv);
                    PlayerPrefs.SetString(Persistence.USER_INFO_STORE_KEY, securedString);
                    LogedUser = _getUserInfoResponse.User;
                }

                ActionHelpers.InvokeSafely(_getUserInfoCompletedCallback, _getUserInfoResponse, true);
                _getUserInfoResponse = null;
            }
        }

        #endregion Private Methods
    }
}