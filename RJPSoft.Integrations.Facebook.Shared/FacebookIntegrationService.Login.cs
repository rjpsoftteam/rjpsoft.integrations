﻿using Facebook;
using Newtonsoft.Json;
using RJPSoft.Integrations.Facebook.CallbacksResponses;
using System;
using System.Linq;
using static RJPSoft.Integrations.Facebook.Constants;
using RJPSoft.Integrations.Facebook.Model;
using UnityEngine;
using RJPSoft.Integrations.Facebook.ExtentionMethods;
using System.Collections.Generic;
using RJPSoft.Security.Cryptography;
using RJPSoft.Integrations.Facebook.Domain;

#if NETFX_CORE
using Windows.Security.Authentication.Web;
#endif

namespace RJPSoft.Integrations.Facebook
{
    /// <summary>
    /// Handles Facebook Integration
    /// </summary>
    public partial class FacebookIntegrationService : Singleton<FacebookIntegrationService>
    {
        #region Private Fields

        private bool _isOAuthFinishedSuccessfully;
        private bool _isOAuthFinishedWithError;
        private string _loginUri;
        private FacebookOAUthData _oauthData;
        private FacebookOAuthResult _oauthResponseData;
        private ResponseStatusCode _oauthResponseStatusCode;
        private Action<FacebookLoginResponse> _loginCompletedCallback;
        private IEnumerable<FacebookPermissions> _requestedPermissions;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Determines whether the user is authenticated.
        /// </summary>
        /// <returns>
        /// <c>true</c> if the is user authenticated; otherwise, <c>false</c>.
        /// </returns>
        public bool IsUserAuthenticated => _oauthData != null && _oauthData.IsUserAuthenticated;

        /// <summary>
        /// Gets the permissions granted by the user.
        /// </summary>
        public HashSet<string> Permissions { get; private set; } = new HashSet<string>();

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Logins to Facebook asynchronously, asking for READ permissions.
        /// </summary>
        /// <param name="readPermissions">The permissions to be asked for the user, must be READ type only</param>
        /// <param name="loginCompletedCallback">Callback that will be called when login finishes</param>
        public void LoginWithReadPermission(IEnumerable<FacebookPermissions> readPermissions, Action<FacebookLoginResponse> loginCompletedCallback = null)
        {
            if (readPermissions == null || !readPermissions.Any())
            {
                throw new InvalidOperationException($"{nameof(readPermissions)} canot be null or empty");
            }
            else if (!readPermissions.AreAllPermissionReadType())
            {
                throw new InvalidOperationException($"{nameof(LoginWithReadPermission)} should not be called with WRITE permissions");
            }
            else
            {
                LoginAsync(readPermissions, loginCompletedCallback);
            }
        }

        /// <summary>
        /// Logins to Facebook asynchronously, asking for WRITE permissions.
        /// </summary>
        /// <param name="readPermissions">The permissions to be asked for the user, must be WRITE tye only</param>
        /// <param name="loginCompletedCallback">Callback that will be called when login finishes</param>
        public void LoginWithWritePermission(IEnumerable<FacebookPermissions> readPermissions, Action<FacebookLoginResponse> loginCompletedCallback = null)
        {
            if (readPermissions == null || !readPermissions.Any())
            {
                throw new InvalidOperationException($"{nameof(readPermissions)} canot be null or empty");
            }
            else if (!readPermissions.AreAllPermissionWriteType())
            {
                throw new InvalidOperationException($"{nameof(LoginWithWritePermission)} should not be called with READ permissions");
            }
            else
            {
                LoginAsync(readPermissions, loginCompletedCallback);
            }
        }

        /// <summary>
        /// Removes the saved token and user info, forcing the user to login again
        /// </summary>
        public void Logout()
        {
            Debug.Log("[FacebookIntegrationService][Logout] - Login out user");
            _client.AccessToken = null;
            _oauthData = null;
            IsInitialied = false;
            foreach (var key in Persistence.ALL_KEYS)
            {
                Debug.Log($"[FacebookIntegrationService][Logout] - Removeing data with key: {key}");
                PlayerPrefs.DeleteKey(key);
            }

            DestroySingleton();
        }

        #endregion Public Methods

        #region Internal Methods

        /// <summary>
        /// Logins the continuation.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="responseStatus">The response status.</param>
        internal void FinishLogin(string responseData, ResponseStatusCode responseStatus)
        {
            _oauthResponseStatusCode = responseStatus;
            _onHideUnity.InvokeSafely(true);

            if (!string.IsNullOrEmpty(responseData))
            {
                Debug.Log($"[FacebookIntegrationService][FinishLogin] - Response Data: {responseData}");
                var responseUri = new Uri(responseData);
                _oauthResponseData = _client.ParseOAuthCallbackUrl(responseUri);

                _isOAuthFinishedSuccessfully = responseStatus == ResponseStatusCode.Success;
                _isOAuthFinishedWithError = responseStatus != ResponseStatusCode.Success;
            }
            else
            {
                _isOAuthFinishedSuccessfully = false;
                _isOAuthFinishedWithError = true;
            }
            
            Debug.Log($"[FacebookIntegrationService][FinishLogin] - Success: {_isOAuthFinishedSuccessfully}");
        }

        #endregion Internal Methods

        #region Private Methods

        private void FireLoginCompletedWithError(ResponseStatusCode statusCode)
        {
            Debug.Log($"[FacebookIntegrationService][FireLoginCompletedWithError] - Login failed with status: {statusCode}");
            FacebookOAuthResultError error = new FacebookOAuthResultError(string.Empty, string.Empty, string.Empty);
            if (_oauthResponseData != null)
            {
                Debug.Log($"[FacebookIntegrationService][FireLoginCompletedWithError] - Error: {_oauthResponseData.Error}");
                error = new FacebookOAuthResultError(_oauthResponseData.Error, _oauthResponseData.ErrorDescription, _oauthResponseData.ErrorReason);
            }

            ActionHelpers.InvokeSafely(_loginCompletedCallback, new FacebookLoginResponse(false, statusCode, error), true);
        }

        private string GetLoginUri(IEnumerable<FacebookPermissions> permissions)
        {
            var callbackUrl = string.Empty;
#if NETFX_CORE
            callbackUrl = WebAuthenticationBroker.GetCurrentApplicationCallbackUri().AbsoluteUri;
#else
            callbackUrl = "https://www.facebook.com/connect/login_success.html";
#endif
            var perm = permissions.ToStringArray();
            var loginUri = _client.GetLoginUrl(
                    new
                    {
                        client_id = _client.AppId,
                        redirect_uri = callbackUrl,
                        scope = permissions.ConcatPermissions(),
                        display = "popup",
                        response_type = "token"
                    });
            Debug.Log("[FacebookIntegrationService][GetLoginUrl] - url: " + loginUri);
            return loginUri.AbsoluteUri;
        }

        private void GetOAUthResultFromFile()
        {
            var serializedOAuthResult = GetSavedDecryptedValue(Persistence.OAUTH_RESULT_STORE_KEY);

            Debug.Log($"[FacebookIntegrationService][GetOAUthResultFromFile] - serializedInfo: {serializedOAuthResult}");

            if (!string.IsNullOrEmpty(serializedOAuthResult))
            {
                var oauthResult = JsonConvert.DeserializeObject<Domain.FacebookOAUthPersistData>(serializedOAuthResult);
                if (oauthResult != null)
                {
                    _oauthData = new FacebookOAUthData(oauthResult);
                }
            }
        }

        private void HandlesLoginFinished()
        {
            if (_isOAuthFinishedSuccessfully)
            {
                _isOAuthFinishedSuccessfully = false;

                if (_oauthResponseData.IsSuccess)
                {
                    Debug.Log($"[FacebookIntegrationService][HandlesLoginFinished] - OK with permissions {_requestedPermissions.ConcatPermissions()}");
                    _oauthData = new FacebookOAUthData(_oauthResponseData);
                    _client.AccessToken = _oauthResponseData.AccessToken;
                    Debug.Log("[FacebookIntegrationService][HandlesLoginFinished] - AccessToken: " + _client.AccessToken);
                    PersistOAUthResult(new FacebookOAUthPersistData(_oauthResponseData));

                    var permissionData = new PermissionsData() { Scopes = _requestedPermissions.ToStringList() };
                    Permissions.AddRange(permissionData.Scopes);
                    _requestedPermissions = new List<FacebookPermissions>();

                    var securedString = CryptoHandler.EncryptAES(JsonConvert.SerializeObject(permissionData),
                        Constants.Security.key, Constants.Security.iv);
                    PlayerPrefs.SetString(Persistence.PERMISSONS_GRANTED_STORE_KEY, securedString);
                    ActionHelpers.InvokeSafely(_loginCompletedCallback, new FacebookLoginResponse(true, ResponseStatusCode.Success), true);
                }
                else
                {
                    Debug.Log("[FacebookIntegrationService][HandlesLoginFinished] - NOK");
                    var hasUserDenied = _oauthResponseData.ErrorReason == FacebookOAuthErrorReasons.user_denied.ToString();
                    FireLoginCompletedWithError(hasUserDenied ? ResponseStatusCode.UserDenied : ResponseStatusCode.GeneralError);
                }
            }
            else if (_isOAuthFinishedWithError)
            {
                _isOAuthFinishedWithError = false;
                Debug.Log("[FacebookIntegrationService][HandlesLoginFinished] - NOK");
                FireLoginCompletedWithError(_oauthResponseStatusCode);
            }
        }

        private void LoginAsync(IEnumerable<FacebookPermissions> permissions, Action<FacebookLoginResponse> loginCompletedCallback = null)
        {
            _loginCompletedCallback = loginCompletedCallback;
            _loginUri = GetLoginUri(permissions);
            if (!IsUserAuthenticated || permissions.Any(p => !Permissions.Contains(p.ToString())))
            {
                Debug.Log("[FacebookIntegrationService][LoginAsync] - User NOT authenticated, Starting Login");
                _requestedPermissions = permissions;
                _onHideUnity.InvokeSafely(false);
                ExecuteLogin();
            }
            else
            {
                Debug.Log("[FacebookIntegrationService][LoginAsync] - User already authenticated, using saved data");
                ActionHelpers.InvokeSafely(_loginCompletedCallback, new FacebookLoginResponse(true, ResponseStatusCode.Success), true);
            }
        }

#if NETFX_CORE
        private ResponseStatusCode ParseWebAuthenticationStatusToResponseStatusCode(WebAuthenticationStatus webAuthenticationStatus)
        {
            Debug.Log($"[FacebookIntegrationService][ParseWebAuthenticationStatusToResponseStatusCode] - status: {webAuthenticationStatus.ToString()}");

            ResponseStatusCode result;
            switch (webAuthenticationStatus)
            {
                case WebAuthenticationStatus.Success:
                    result = ResponseStatusCode.Success;
                    break;

                case WebAuthenticationStatus.UserCancel:
                    result = ResponseStatusCode.UserCancel;
                    break;

                case WebAuthenticationStatus.ErrorHttp:
                    result = ResponseStatusCode.ErrorHttp;
                    break;

                default:
                    result = ResponseStatusCode.GeneralError;
                    break;
            }

            return result;
        }
#endif

        private void PersistOAUthResult(Domain.FacebookOAUthPersistData oauthResult)
        {
            var serializedOAuhResult = JsonConvert.SerializeObject(oauthResult);
            Debug.Log($"[FacebookIntegrationService][PersistOAUthResult] - serializedInfo: {serializedOAuhResult}");
            var securedString = CryptoHandler.EncryptAES(serializedOAuhResult, Constants.Security.key, Constants.Security.iv);
            PlayerPrefs.SetString(Persistence.OAUTH_RESULT_STORE_KEY, securedString);
        }

        #endregion Private Methods
    }
}