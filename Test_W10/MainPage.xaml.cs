﻿using RJPSoft.Integrations.Facebook;
using RJPSoft.Integrations.Facebook.EventArgs;
using RJPSoft.Integrations.Facebook.Model;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Test_W10
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        FacebookIntegrationService _facebookService;

        public MainPage()
        {
            this.InitializeComponent();
            _facebookService = FacebookIntegrationService.GetInstance("196078490889366");
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            var callback = FacebookIntegrationService.GetApplicationCallbackUri();
            var permissions = new FacebookPermissions[] 
            {
                FacebookPermissions.PUBLIC_PROFILE,
                FacebookPermissions.USER_FRIENDS,
                FacebookPermissions.publish_actions,
                FacebookPermissions.user_photos
            };

            var loginUrl = _facebookService.GetLoginUri(callback.AbsoluteUri, permissions);
            _facebookService.LoginCompleted += _facebookService_LoginCompleted;
            _facebookService.LoginAsync(loginUrl);
        }

        private void _facebookService_LoginCompleted(object sender, FacebookLoginEventArgs e)
        {
            _facebookService.LoginCompleted -= _facebookService_LoginCompleted;
            textBlockSuccess.Text = e.Success.ToString();
            textBlockStatusCode.Text = e.StatusCode.ToString();

            if (e.Error != null)
            {
                textBlockError.Text = e.Error.Error;
                textBlockErrorDescription.Text = e.Error.ErrorDescription;
                textBlockErrorReason.Text = e.Error.ErrorReason;
            }
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {

            //var logoutUri = this.Client.GetLogoutUrl(new
            //{
            //    next = FACEBOOK_LOGIN_SUCCESS_URL,
            //    app_key = AppID,
            //    access_token = this.Client.AccessToken
            //});

            //webAuthenticationRepository.AuthenticateAndContinue(logoutUri, new Uri(FACEBOOK_LOGIN_SUCCESS_URL));
        }
    }
}
